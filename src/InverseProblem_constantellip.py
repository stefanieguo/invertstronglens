# -*- coding: utf-8 -*-
"""
Created on Sat May 14 14:19:50 2016

@author: xguo
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:34 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm
from scipy.ndimage.filters import gaussian_filter
from astropy.convolution import convolve, Gaussian2DKernel
import functools
from slicesample import slicesample

plt.rcParams['image.cmap'] = 'inferno'

gauss_kernel = Gaussian2DKernel(2)

re_min = 2#@0.5
re_max = 8
xLc_min, xLc_max = -2, 2
yLc_min, yLc_max = -2, 2
xSc_min, xSc_max = -2, 2
ySc_min, ySc_max = -2, 2
s0_min, s0_max = 0, 5
aS_min, aS_max = 0.1, 3
eS_min, eS_max = 0, 1
LARGENEG = -1e10

def generate_model(X, Y, params):
    re, xLc, yLc, xSc, ySc, thetaS, aS, eS, s0 = params
    lens  = lenses.SIS([re, xLc, yLc], False)
    f = functools.partial(srcprf.ConstantEllipse, xSc = xSc, ySc=ySc, theta=thetaS, a=aS, e=eS, s0=s0)
    return forward.LensFunction(X, Y, lens, f)

def logprior(params, datamax):
    re, xLc, yLc, xSc, ySc, thetaS, aS, eS, s0 = params
    
    if (re < re_min or re>re_max
        or xLc<xLc_min or xLc > xLc_max
        or yLc<yLc_min or yLc > yLc_max
        or xSc<xSc_min or xSc > xSc_max
        or ySc<ySc_min or ySc > ySc_max
        or eS<eS_min or eS > eS_max
        or aS<aS_min or aS > aS_max
        or s0<s0_min or s0 > s0_max
        ):
        return LARGENEG
    else:
        return 0#np.sum( -( np.array([xLc, yLc, xSc, ySc]) - np.array([xLc_mean, yLc_mean, xSc_mean, ySc_mean]) )**2/(
            #np.array([xLc_sigma, xLc_sigma, xSc_sigma, ySc_sigma])**2/2) )

def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior( params, X, Y, data, PeakSoverN):
    model = generate_model(X, Y, params)
    datamax = data.max()
    return loglikelihood(model, data, PeakSoverN) + logprior(params, datamax)
    


if __name__ == "__main__":
    
    #generate data
    zL_true = 0.3
    zS_true = 0.7
    sigma_true = 600
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    xSc_true, ySc_true =  1.0, 1.5
    thetaS_true = 40*np.pi/180
    aS_true = 1.5
    eS_true = 0.4
    s0_true = 1
    
    param_true =  np.array([re_true, xLc_true, yLc_true,   
                  xSc_true, ySc_true,  thetaS_true, aS_true, eS_true, s0_true] )
    PeakSoverN = 20
    #Image grid
    X, Y = np.meshgrid( np.linspace(-12, 12, 64), np.linspace(-12, 12, 64)  )
    
    data = generate_model(X, Y, param_true)     
    #data = convolve(data, gauss_kernel)
    data += forward.add_noise(data, PeakSoverN)
    
    # construct log posterior based on data, grid, and noise level
    logpdf = functools.partial(logposterior, X=X, Y=Y, data=data, PeakSoverN = PeakSoverN)    
    
    #show mock data
    plt.figure(4)
    plt.clf()   
    plt.pcolor(X,Y, data)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.title('Mock lensing data with S/N={:.0f}'.format(PeakSoverN))
    plt.show()
    # %%
    # initial guess
    re_g = 3.
    xLc_g, yLc_g = 0,0
    thetaL_g = 0 
    xSc_g, ySc_g = 0,0
    thetaS_g = 0
    s0_g = 2
    aS_g = 1
    eS_g = 0.1    
    
    param_g = np.array( [re_g, xLc_true, yLc_true,  
                  xSc_true, ySc_true,  thetaS_g, aS_g, eS_g, s0_g] )
    
         
    Nsample = np.int(2e4)
    
    samples, neval = slicesample(param_true, Nsample, logpdf, thin=1, burnin = 500, width = [2, 0.5, 0.5, 0.5, 0.5, np.pi/3, 0.5, 0.1, 1])    
    
    colthetaS = 5
    samples[colthetaS ,:] =  samples[colthetaS ,:]  % np.pi
#%%
    

    plt.figure(1)
    plt.clf()
    param_map = np.zeros(param_true.shape)
    for i in range(param_true.size): 
        plt.subplot(3,3, i+1)
        n, b, _ = plt.hist( samples[:,i], bins = 50 )
        param_map[i] = b[np.argmax(n)]
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
    
    
    plt.figure(3)
    plt.clf()
    data_map = generate_model(X, Y, param_map)
    plt.pcolor(X, Y, data_map)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.show()
    
    #%%
    import corner
    
    cplot = corner.corner(samples[:, [0, 6, 7, 8]], bins = 25, 
                  quantiles = [0.16,0.5,  0.84], 
                range = [0.9999]*4,
                  show_titles = True, 
                  truths = [re_true, aS_true, eS_true, s0_true],
                    verbose = True, 
                title_kwargs={"fontsize": 20},
                label_kwargs={"fontsize": 20},
        labels=[r"$b$", r"$a_S$", r"$e_S$",
                                     r"$s_0$"])
    plt.show()
