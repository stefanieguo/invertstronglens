# -*- coding: utf-8 -*-
"""
Created on Tue May 17 01:45:14 2016

@author: xguo
"""

# -*- coding: utf-8 -*-
"""
Created on Sat May 14 15:25:01 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
from misc import draw_rect_from_corner
plt.rcParams['image.cmap'] = 'inferno'

def f(x):
    return np.exp( -x**2/2)*(1+(np.sin(3*x))**2)*( 1 + (np.cos(5*x))**2)
def logf(x):
    return (-x**2/2)+np.log(1+(np.sin(3*x))**2)+ np.log(  1 + (np.cos(5*x))**2)
    
def log2df(z):
    x, y = z
    return (-(x-1)**2/2 - (y-2)**2/2/0.5**2)#+np.log(1+(np.sin(3*x))**2)+ np.log(  1 + (np.cos(5*y))**2)

def slicesample(initial, nsamples, logpdf, thin=1, burnin=0, width = 10, bar_length = 40, plot_real = False, generate_model_part=[]):
    maxiter = 200
    dim = initial.shape[0]
    rnd = np.zeros( (nsamples, dim) )
    neval = nsamples
    e = np.random.exponential(1, (nsamples*thin+burnin, 1))
    RW = np.random.rand(  nsamples*thin+burnin, dim )
    RD = np.random.rand(  nsamples*thin+burnin, dim )
    x0 = initial
    
        
    for i in range(-burnin, nsamples*thin):
        plt.plot(x0, 'ko')
        z = logpdf(x0) - e[i+burnin]
        r = width*RW[i+burnin, :]
        xl = x0 - r
        xr = xl + width
        niter = 0
        
        
        xp = RD[i+burnin, :]*(xr - xl) + xl
        plt.plot(xp, 'co')
        rec1 = draw_rect_from_corner(xl, xr, color = 'b')
        plt.pause(0.5)
        niter = 0
        while ( (not logpdf(xp) > z)  and (niter < maxiter) ):
            rshrink = xp>x0;
            xr[rshrink] = xp[rshrink]
            draw_rect_from_corner(xl, xr, color = 'y', linestyle = '--')
            plt.pause(0.5)            
            lshrink = np.logical_not(rshrink)
            xl[lshrink] = xp[lshrink]
            draw_rect_from_corner(xl, xr, linestyle = '--')
            
            xp = np.random.rand(dim)*(xr-xl) + xl
            plt.plot(xp, 'co')
            plt.pause(0.5)
            niter += 1
        if (niter >= maxiter):
            raise Exception('ErrShrinkin')
        plt.plot( [x0[0], xp[0]], [x0[1], xp[1]], 'k')
        plt.pause(0.5)
        x0 = xp[:]
        
        if (i >= 0 and i%thin == 0):
            rnd[np.int(i/thin),:] = x0[:]
        neval += niter
        plt.pause(1)
        #show progress bar
        
            
            
        
    neval = neval*1.0/(nsamples*thin+burnin)
    print('\nDone!')
    return rnd, neval
    
if __name__ == '__main__':
    nsamples = 5
    thin = 1
    burnin = 0
    width = 2.5
    initial = np.array([1., 1.])
    
    
    #%%
    plt.figure(1)
    plt.clf()
    X, Y = np.meshgrid( np.linspace(-2, 3, 100), np.linspace(-1,4, 100))
    Z = np.exp(log2df((X,Y)))
    plt.contour(X,Y, Z)
    plt.ion()
#    
#    plt.figure(2)
#    plt.clf()
    rnd, neval = slicesample(initial, nsamples, log2df,thin, burnin, width = 3)
#    plt.hist2d(rnd[:,0],rnd[:,1], bins=[ X[0,:], Y[:,0]] )
#    plt.show()