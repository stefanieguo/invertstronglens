# -*- coding: utf-8 -*-
"""
Created on Mon May 16 21:14:47 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
import functools
from slicesample import slicesample
from timeit import default_timer as timer
from InverseProblem_SIEGauss import generate_model
import math

if __name__ == "__main__":
#    fname = "/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/SIEGauss_sample.npy"
#    samples = np.load(fname)
#    
#    posterior_mean = np.percentile(samples, 50, axis=0)
#    posterior_min = np.percentile(samples, 16, axis=0)
#    posterior_max = np.percentile(samples, 84, axis=0)
#    #re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true,  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true
#    for i in range(len(posterior_mean)):
#        if (i==5 or i==8):
#            print('{:.2f}  {:.2f} {:.2f}'.format(posterior_mean[i]/np.pi, -posterior_mean[i]/np.pi + posterior_max[i]/np.pi, posterior_mean[i]/np.pi - posterior_min[i]/np.pi))
#        else:
#            print('{:.2f}  {:.2f} {:.2f}'.format(posterior_mean[i], -posterior_mean[i] + posterior_max[i], posterior_mean[i] - posterior_min[i]))
#            
    
#    fname = "/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/output/SIEpixel/samples_2.npy"
#    samples = np.load(fname)
#    samples = samples[:,0:6]
#    posterior_mean = np.percentile(samples, 50, axis=0)
#    posterior_min = np.percentile(samples, 16, axis=0)
#    posterior_max = np.percentile(samples, 84, axis=0)
#    #re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true,  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true
#    for i in range(len(posterior_mean)):
#        if (i==5 or i==8):
#            print('{:.3f}  {:.3f} {:.3f}'.format(posterior_mean[i]/np.pi, -posterior_mean[i]/np.pi + posterior_max[i]/np.pi, posterior_mean[i]/np.pi - posterior_min[i]/np.pi))
#        else:
#            print('{:.3f}  {:.3f} {:.3f}'.format(posterior_mean[i], -posterior_mean[i] + posterior_max[i], posterior_mean[i] - posterior_min[i]))
            
    fname = "/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/output/SIEpixel/samples_2.npy"
    samples = np.load(fname)
    samples = samples[:,6:]
    mean_source = np.mean(samples, axis=0)
    
    std_source = np.std(samples, axis=0)
    
    plt.figure(5)
    plt.clf()
    xaxis = np.linspace(-0.8, 0.8, 12)
    yaxis = np.linspace(-0.8, 0.8, 12)
    Xs, Ys = np.meshgrid(xaxis, yaxis)
    plt.pcolor(Xs, Ys, std_source.reshape(xaxis.size, yaxis.size))
    plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
    plt.title('Std of posterior of source pixel values', fontsize = 18)
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.colorbar()