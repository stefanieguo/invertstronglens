# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:34 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
from astropy.convolution import convolve, Gaussian2DKernel
import functools
from slicesample import slicesample
from timeit import default_timer as timer

plt.rcParams['image.cmap'] = 'inferno'

gauss_kernel = Gaussian2DKernel(1)

re_min = 0.5
re_max = 3
xLc_min, xLc_max = -1.5, 1.5
yLc_min, yLc_max = -1.5, 1.5
xSc_min, xSc_max = -1.5, 1.5
ySc_min, ySc_max = -1.5, 1.5
s0_min, s0_max = 0, 5
aS_min, aS_max = 0.01, 0.5
qS_min, qS_max = 0, 1
nS_min, nS_max = 0.5, 10
LARGENEG = -1e10

def generate_model(X, Y, params, verbose=False):
    re, xLc, yLc, xSc, ySc, thetaS, aS, qS, s0, nS = params
    lens  = lenses.SIS([re, xLc, yLc], verbose)
    if (verbose):
        lens.drawCrit()
        lens.drawCaustic()
    f = functools.partial(srcprf.Sersic, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=aS, qS=qS, s0=s0, nS=nS)
    return forward.LensFunction(X, Y, lens, f)

def logprior(params, datamax):
    re, xLc, yLc, xSc, ySc, thetaS, aS, qS, s0, nS = params
    
    if (re < re_min or re>re_max
        or xLc<xLc_min or xLc > xLc_max
        or yLc<yLc_min or yLc > yLc_max
        or xSc<xSc_min or xSc > xSc_max
        or ySc<ySc_min or ySc > ySc_max
        or qS<qS_min or qS > qS_max
        or aS<aS_min or aS > aS_max
        or s0<s0_min or s0 > s0_max
        or nS<nS_min or nS > nS_max
        ):
        return LARGENEG
    else:
        return 0#np.sum( -( np.array([xLc, yLc, xSc, ySc]) - np.array([xLc_mean, yLc_mean, xSc_mean, ySc_mean]) )**2/(
            #np.array([xLc_sigma, xLc_sigma, xSc_sigma, ySc_sigma])**2/2) )

def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior( params, X, Y, data, PeakSoverN):
    model = generate_model(X, Y, params)
    model = gaussian_filter(model, sigma = 1)
    #model = convolve(model, gauss_kernel)
    datamax = data.max()
    return loglikelihood(model, data, PeakSoverN) + logprior(params, datamax)
    


if __name__ == "__main__":
    
    #generate data
    zL_true = 0.3
    zS_true = 0.7
    sigma_true = 600/2
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    xSc_true, ySc_true =  0.2, 0.6
    thetaS_true = 10*np.pi/180
    aS_true = 0.4
    qS_true = 0.6
    nS_true = 3.9
    s0_true = 1
    
    
    # initial guess
    re_g = 1.
    xLc_g, yLc_g, xSc_g, ySc_g = 0,0, 0, 0
    thetaS_g = 0    
    aS_g = 0.2
    qS_g = 0.9    
    nS_g = 4
    s0_g = 2
    
    #width 
    re_w = 0.5
    xLc_w, yLc_w, xSc_w, ySc_w = 0.1, 0.1, 0.1, 0.1
    thetaS_w = np.pi/4
    aS_w = 0.3
    qS_w = 0.1
    nS_w = 0.2
    s0_w = 1
    
    
    param_true =  np.array([re_true, xLc_true, yLc_true,   
                  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true, nS_true] )
    PeakSoverN = 25
    #Image grid
    X, Y = np.meshgrid( np.linspace(-3, 3, 64), np.linspace(-3, 3, 64)  )
    plt.figure(2)
    plt.clf()
    data = generate_model(X, Y, param_true, True)  
    st = timer()
    data = gaussian_filter(data, sigma = 1)
    print(timer() - st)
    data += forward.add_noise(data, PeakSoverN)
    
    # construct log posterior based on data, grid, and noise level
    logpdf = functools.partial(logposterior, X=X, Y=Y, data=data, PeakSoverN = PeakSoverN)    
    
    #show mock data
       
    ax1 = plt.pcolor(X,Y, data)
    #ax1.set_norm(LogNorm())
    draw_ellipse(aS_true, aS_true*qS_true, xSc_true, ySc_true, thetaS_true, color = 'r')
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.title('Mock lensing data with S/N={:.0f}'.format(PeakSoverN))
    plt.show()
    # %%
    
    param_g = np.array( [re_g, xLc_g, yLc_g,  
                  xSc_g, ySc_g,  thetaS_g, aS_g, qS_g, s0_g, nS_g ] )
    
    param_w = np.array( [re_w, xLc_w, yLc_w,  
                  xSc_w, ySc_w,  thetaS_w, aS_w, qS_w, s0_w, nS_w] )
    Nsample = np.int(2e4)
    
    samples, neval = slicesample(param_true, Nsample, logpdf, thin=1, burnin = int(Nsample/10), width = param_w)    
    
    colthetaS = 5
    samples[colthetaS ,:] =  samples[colthetaS ,:]  % np.pi
#%%
    

    plt.figure(1)
    plt.clf()
    param_map = np.zeros(param_true.shape)
    for i in range(param_true.size): 
        plt.subplot(3,4, i+1)
        n, b, _ = plt.hist( samples[:,i], bins = 50 )
        param_map[i] = b[np.argmax(n)]
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
    
    
    plt.figure(3)
    plt.clf()
    data_map = generate_model(X, Y, param_map)
    plt.pcolor(X, Y, data_map)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.show()
    
    #%%
    import corner
    
    cplot = corner.corner(samples[:, [0, 6, 7, 8, 9]], bins = 25, 
                  quantiles = [0.16,0.5,  0.84], 
                range = [0.9999]*5,
                  show_titles = True, 
                  truths = [re_true, aS_true, qS_true, s0_true,  nS_true],
                    verbose = True, 
                title_kwargs={"fontsize": 20},
                label_kwargs={"fontsize": 20},
        labels=[r"$b$", r"$a_S$", r"$q_S$",
                                     r"$s_0$", r"$n_S$"])
    plt.show()
