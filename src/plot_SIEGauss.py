# -*- coding: utf-8 -*-
"""
Created on Mon May 16 15:35:55 2016

@author: xguo
"""


import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
import functools
from slicesample import slicesample
from timeit import default_timer as timer
from InverseProblem_SIEGauss import generate_model
import math

if __name__ == "__main__":
    fname = "/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/SIEGauss_sample0.npy"
    samples = np.load(fname)
    #%%
    xmin, xmax = -4, 4
    ymin, ymax = -4, 4
    nx, ny = 64, 64
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)  )
    
    zL_true = 0.3
    zS_true = 0.72
    sigma_true = 350
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    thetaL_true = 0.2*np.pi 
    ellipL_true = 0.3
    sL_true  = 0.1
    
    xSc_true, ySc_true =  0.1, 0
    thetaS_true = 130*np.pi/180
    aS_true = 0.2
    qS_true = 0.125/0.2
    s0_true = 1
    param_true =  np.array([re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true, 
                  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true] )
                  
                  
    re_min = 0
    re_max = min( (ymax - ymin)/2, (xmax - xmin)/2)
    ellipL_min, ellipL_max = 0, 1
    sL_min, sL_max = 0, 1
    thetaL_min, thetaL_max = -math.inf, math.inf
    xSc_min, xSc_max, xLc_min, xLc_max = xmin/2, xmax/2, xmin/2, xmax/2
    ySc_min, ySc_max, yLc_min, yLc_max = ymin/2, ymax/2, ymin/2, ymax/2
    thetaS_min, thetaS_max = -math.inf, math.inf
    aS_min, aS_max = 0, min( (ymax - ymin)/4, (xmax - xmin)/4)
    qS_min, qS_max = 0, 1
    s0_min, s0_max = 0, 5
    
    param_bounds = np.array([ [re_min, re_max],  
                             [xLc_min, xLc_max],
                             [yLc_min, yLc_max],
                             [ellipL_min, ellipL_max], 
                             [sL_min, sL_max],
                             [thetaL_min, thetaL_max],
                             [xSc_min, xSc_max],
                             [ySc_min, ySc_max],
                             [thetaS_min, thetaS_max],
                             [aS_min, aS_max],
                             [qS_min, qS_max], 
                             [s0_min, s0_max]
                             ])
    
    # %%
    param_toplot = [0, 3, 4,  9, 10, 11]
    labels=[r"$b$", r"$e_L$",r"$s_L$",
        r"$a_S$", r"$q_S$", r"$s_0$"]
    col = len(param_toplot)
    
    K = len(param_toplot)
    factor = 2.0           # size of one side of one panel
    lbdim = 0.5 * factor   # size of left/bottom margin
    trdim = 0.2 * factor   # size of top/right margin
    whspace = 0.05         # w/hspace size
    plotdim = factor * K + factor * (K - 1.) * whspace
    dim = lbdim + plotdim + trdim
    re_w = (re_max - re_min)/5
    xLc_w, yLc_w, xSc_w, ySc_w = (xLc_max - xLc_min)/5, (yLc_max - yLc_min)/5, (xSc_max - xSc_min)/5, (ySc_max - ySc_min)/5
    ellipL_w = (ellipL_max - ellipL_min)/5
    sL_w = (sL_max - sL_min)/10
    thetaL_w = np.pi/4
    thetaS_w = np.pi/4
    aS_w = (aS_max - aS_min)/5
    qS_w = (qS_max - qS_min)/5
    s0_w = (s0_max - s0_min)/5
    param_w = np.array( [re_w, xLc_w, yLc_w,  ellipL_w, sL_w, thetaL_w, 
                  xSc_w, ySc_w,  thetaS_w, aS_w, qS_w, s0_w] )
   
    # Create a new figure if one wasn't provided.
    
#    fig, axes = plt.subplots(K, K, figsize=(dim, dim))
#    
#    # Format the figure.
#    lb = lbdim / dim
#    tr = (lbdim + plotdim) / dim
#    fig.subplots_adjust(left=lb, bottom=lb, right=tr, top=tr,
#                        wspace=whspace, hspace=whspace)
    #%%
    plt.figure(10, figsize = (dim, dim))
    plt.clf()
    plt.ion()
    plt.show()
                
    for i, parami in enumerate(param_toplot[:-1]):
        for j, paramj in enumerate(param_toplot[:]):
            if (j>i):
                ax = plt.subplot(K, K,j*col+i+1)
                plt.plot(param_true[parami],param_true[paramj], 'r*', markersize = 15)
                if (j == 5):
                    ax.set_xlim( [param_true[parami] - 1*param_w[parami], param_true[parami] + 1*param_w[parami]])
                    ax.set_ylim( [param_true[paramj] - 2*param_w[paramj], param_true[paramj] + 2*param_w[paramj]] )
                elif (j==2):
                    ax.set_xlim( [param_true[parami] - 1*param_w[parami], param_true[parami] + 1*param_w[parami]])
                    ax.set_ylim( [param_true[paramj] - 0.7*param_w[paramj], param_true[paramj] + 0.7*param_w[paramj]] )
                else:
                    ax.set_xlim( [param_true[parami] - 1*param_w[parami], param_true[parami] + 1*param_w[parami]])
                    ax.set_ylim( [param_true[paramj] - 1*param_w[paramj], param_true[paramj] + 1*param_w[paramj]] )
                if (j<K-1):
                    ax.set_xticklabels([])
                    
                else:
                    ax.set_xlabel(labels[i], fontsize = 20)
                    ax.tick_params(labelsize = 10)
                if (i>0):
                    ax.set_yticklabels([])
                else:
                    ax.set_ylabel(labels[j], fontsize = 20)
                    ax.tick_params(labelsize = 10)
    #%%            
    count = 0
    Nsample0 = np.int(5e4)
    burnin = int(Nsample0/10)
    thin = 2
    Nsample = Nsample0*thin + burnin
    
    
    step = 5
    for dataind in range(0, 700 , step):
        for i, parami in enumerate(param_toplot[:-1]):
            for j, paramj in enumerate(param_toplot[:]):
                if (j>i):
                    plt.subplot(K, K,j*col+i+1)
                    plt.plot(samples[:dataind, parami], samples[:dataind, paramj], color='b', linewidth = 1)
            
        plt.subplot(2, 2, 2)
        plt.pcolor(X, Y, generate_model(X, Y, samples[dataind,:]))
        plt.xlabel('x [arcsec]', fontsize = 20)
        plt.ylabel('y [arcsec]', fontsize = 20)
        plt.suptitle('#iter={:06d}'.format(dataind), fontsize = 25)
        #plt.pause(0.1)
        #plt.savefig('anim/SIEGauss/image{:06d}'.format(count))
        count +=1
        print('fine stage', count)
    #%%  
    step = 3000
    for dataind in range(700,  Nsample , 3000):
        for i, parami in enumerate(param_toplot[:-1]):
            for j, paramj in enumerate(param_toplot[:]):
                if (j>i):
                    plt.subplot(K, K,j*col+i+1)
                    plt.plot(samples[:dataind, parami], samples[:dataind, paramj], color='b', linewidth = 1)
                
            
        plt.subplot(2, 2, 2)
        plt.pcolor(X, Y, generate_model(X, Y, samples[dataind,:]))
        plt.xlabel('x [arcsec]', fontsize = 20)
        plt.ylabel('y [arcsec]', fontsize = 20)
        plt.suptitle('#iter={:06d}'.format(dataind), fontsize = 25)
        #plt.pause(0.1)
        #plt.savefig('anim/SIEGauss/image{:06d}'.format(count))
        count +=1
        print('coarse stage', count)
        