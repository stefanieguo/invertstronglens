# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 19:15:08 2016

@author: xguo
"""



import numpy as np
from scipy.integrate import quad
import matplotlib.pyplot as plt

def oneOverEz(z, Omegam = 0.286, OmegaLambda = 0.714):
    """
    Helper function for converting z to distance
    We assume flat Universe with standard cosmological parameter
    """
    return 1./np.sqrt(Omegam*(1+z)**3 + OmegaLambda)
    
def z2dAMpc(z, H0=69.6, Omegam = 0.286, OmegaLambda = 0.714 ):
    """
    Convert redshift z to angular-diameter distance in units of Mpc
    """
    c = 3.e8
    dH = c/H0 #Hubble distance in Mpc
    return dH/(1+z)*quad(oneOverEz, 0, z, args=(Omegam, OmegaLambda) )[0]
    
def dLS(zL, zS, H0=69.6, Omegam = 0.286, OmegaLambda = 0.714):
    """
    Returns dLS for given redshift of lens and source
    """
    c = 3.e8
    dH = c/H0 #Hubble distance in Mpc
    return dH*(1+zS)/(1+zL)*quad(oneOverEz, zL, zS, args=(Omegam, OmegaLambda) )[0]

def dLSOverdS(zL, zS, H0=69.6, Omegam = 0.286, OmegaLambda = 0.714):
    return dLS(zL, zS, H0, Omegam, OmegaLambda)/z2dAMpc(zS, H0, Omegam, OmegaLambda)
    
def return_re(zL, zS, sigma):
    return 28.8*(zS-zL)/zS*(sigma/1000)**2
    
def rotateByTheta(x, y, theta):
    """
    Rotate [x,y] by angle theta from x axis
    """
    return np.cos(theta)*x - np.sin(theta)*y, np.sin(theta)*x + np.cos(theta)*y
    
def return_ellipse(a, b, x0, y0, theta):
    theta_ar = np.linspace(0, np.pi*2, 100)
    x, y = np.cos(theta_ar)*a , np.sin(theta_ar)*b 
    x, y = rotateByTheta(x, y, theta)
    x+= x0
    y+= y0
    return x, y
    
def draw_ellipse(a, b, x0, y0, theta, color = 'r', linestyle = '-', linewidth = 2):
    x, y = return_ellipse(a, b, x0, y0, theta)
    return plt.plot(x, y, color, linestyle = linestyle, linewidth = linewidth)
    
def draw_rect(xmin, ymin, width, height, color = 'r', linestyle = '-'):
    plt.plot( [xmin, xmin+width], [ymin, ymin] , color = color, linestyle = linestyle)
    plt.plot( [xmin, xmin+width], [ymin+height, ymin+height] , color = color, linestyle = linestyle)
    plt.plot( [xmin, xmin], [ymin, ymin+height] , color = color, linestyle = linestyle)
    plt.plot( [xmin+width, xmin+width], [ymin, ymin+height] , color = color, linestyle = linestyle)
    
def draw_rect_from_corner(xl, xr, color = 'r', linestyle = '-'):
    xmin = xl[0]
    ymin = xl[1]
    width = xr[0] - xmin
    height = xr[1] - ymin
    plt.plot( [xmin, xmin+width], [ymin, ymin] , color = color, linestyle = linestyle)
    plt.plot( [xmin, xmin+width], [ymin+height, ymin+height] , color = color, linestyle = linestyle)
    plt.plot( [xmin, xmin], [ymin, ymin+height] , color = color, linestyle = linestyle)
    plt.plot( [xmin+width, xmin+width], [ymin, ymin+height] , color = color, linestyle = linestyle)
    
    
def cart2pol(x, y):
    """
    Convert Cartesian coords (x,y) to polar coords (rho, phi)
    """
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)     
    return(rho, phi)

    
    
if __name__ == '__main__':
    print( z2dAMpc(3) )
    print( dLS(0.2, 0.4)/z2dAMpc(0.4))