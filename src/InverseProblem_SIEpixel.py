# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:34 2016

@author: xguo
"""


import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
import functools
from slicesample import slicesample
from timeit import default_timer as timer
import math

plt.rcParams['image.cmap'] = 'inferno'

source_max = 8
LARGENEG = -1e25
NUM_LENS_PARAM = 6
SDARK = 0.1
SBRIGHT = 8

def generate_model(X, Y, sx, sy, params, verbose=False):
    re, xLc, yLc, ellipL, sL, thetaL = params[0:NUM_LENS_PARAM]
    lens  = lenses.SIE([re, xLc, yLc, ellipL, sL, thetaL], verbose)
    if (verbose):
        lens.drawCrit()
        lens.drawCaustic()
    model = forward.LensImage(X, Y, lens, sx, sy, params[NUM_LENS_PARAM:])
    model = gaussian_filter(model, sigma = 1)
    return model

def logprior_general(param, param_bounds):
    if ( np.any( param[0:NUM_LENS_PARAM]<param_bounds[0:NUM_LENS_PARAM,0]) 
        or np.any( param[0:NUM_LENS_PARAM]>param_bounds[0:NUM_LENS_PARAM,1] )
        or np.any( param[NUM_LENS_PARAM:] < 0) 
        or np.any( param[NUM_LENS_PARAM:] > source_max) ) : 
        return LARGENEG
    else:
        #prior for pixel being dark    
        return 0#np.sum( np.log(0.5*np.exp( - param[NUM_LENS_PARAM:]/SDARK**2) + 0.5*np.exp(- param[NUM_LENS_PARAM:]/SBRIGHT**2)) )
            
def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior( params, X, Y, sx, sy, data, PeakSoverN, logprior):
    model = generate_model(X, Y, sx, sy, params)
    return loglikelihood(model, data, PeakSoverN) + logprior(params)
    


if __name__ == "__main__":
    #image parameters
    xmin, xmax = -2.5, 2.5
    ymin, ymax = -2.5, 2.5
    nx, ny = 64, 64
    #generate data
    zL_true = 0.3
    zS_true = 0.6
    sigma_true = 275
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    thetaL_true = 0.2*np.pi 
    ellipL_true = 0.3
    sL_true  = 0.05
    
    
    re_min, re_max = 0, min( (ymax - ymin)/2, (xmax - xmin)/2)
    ellipL_min, ellipL_max = 0, 1
    sL_min, sL_max = 0, 0.2
    thetaL_min, thetaL_max = -math.inf, math.inf
    xLc_min, xLc_max = xmin/2, xmax/2
    yLc_min, yLc_max = ymin/2, ymax/2
    
    PeakSoverN = 50
    
    xaxis = np.linspace(-0.8, 0.8, 12)
    yaxis = np.linspace(-0.8, 0.8, 12)
    Xs, Ys = np.meshgrid(xaxis, yaxis)
    xSc1, ySc1 = 0.2, -0.2
    xSc2, ySc2 =-0.2, 0.05
    thetaS1, thetaS2 = 0.3*np.pi, 0.8*np.pi
    aS1, aS2 = 0.2, 0.1
    qS1, qS2 = 0.6, 0.8
    S = srcprf.EllipticalGaussian(Xs, Ys, xSc1, ySc1, thetaS1, aS1, qS1, 5) + srcprf.EllipticalGaussian(Xs, Ys,  xSc2, ySc2, thetaS2, aS2, qS2, 7) 
    
    
    param_bounds = np.array([ [re_min, re_max],  
                             [xLc_min, xLc_max],
                             [yLc_min, yLc_max],
                             [ellipL_min, ellipL_max], 
                             [sL_min, sL_max],
                             [thetaL_min, thetaL_max]
                             ])
                             
    param_true =  np.append( np.array([re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true] ), S.ravel() ) 
    
    #Image grid
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)  )
    #generate_data
    data = generate_model(X, Y, xaxis, yaxis, param_true, True)  
    data += forward.add_noise(data, PeakSoverN)
    #%%
    logprior =  functools.partial(logprior_general, param_bounds = param_bounds)
           
    #width 
    re_w = (re_max - re_min)/5
    xLc_w, yLc_w = (xLc_max - xLc_min)/5, (yLc_max - yLc_min)/5
    ellipL_w = (ellipL_max - ellipL_min)/5
    sL_w = (sL_max - sL_min)/2
    thetaL_w = np.pi/4
    
    param_w = np.append( np.array( [re_w, xLc_w, yLc_w,  ellipL_w, sL_w, thetaL_w] )*2, np.ones(S.ravel().shape)*5 ) 
    param_g = param_true + np.random.normal(0, 1)*param_w
    while ( logprior(param_g) <= LARGENEG):
        param_g = param_true + np.random.normal(0, 1)*param_w/2
    #fix the initial image at very dark
    param_g[NUM_LENS_PARAM:] = 0.2
    #start the initial guess of re at reasonable number
    param_g[0] = 1.1 
#    param_g[1] = 0.
#    param_g[2] = 0.
#    param_g[3] = 0.2
#    param_g[4] = 0.06
#    param_g[5] = 0
#    
    # construct log posterior based on data, grid, and noise level
    logpdf = functools.partial(logposterior, X=X, Y=Y, sx = xaxis, sy=yaxis, data=data, PeakSoverN = PeakSoverN, logprior = logprior)    
    
    
    plt.figure(2)
    plt.clf()    
    #show mock data       
    ax1 = plt.pcolor(X,Y, data)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.title('Mock lensing data with S/N={:.0f}'.format(PeakSoverN))
    
    plt.figure(3)
    plt.clf()
    data_g = generate_model(X, Y, xaxis, yaxis, param_g, True)  
    plt.pcolor(X,Y, data_g)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.title('initial guess', fontsize = 16)    
    plt.show()
    
    
    # %%
    Nsample0 = np.int(9e4)
    burnin = np.int(Nsample0/5)
    thin = 1
    Nsample = Nsample0*thin + burnin
    samples0, neval = slicesample(param_g, Nsample, logpdf, thin=1, burnin = 0, width = param_w)    
    samples = samples0[burnin::thin,:]
    # make the angle within 0, pi
    
    colthetaL = 5
    samples[colthetaL ,:] =  samples[colthetaL ,:]  % np.pi
#%%
    plt.figure(1)
    plt.clf()
    param_map = np.zeros(NUM_LENS_PARAM)
    for i in range(NUM_LENS_PARAM): 
        plt.subplot(3,2, i+1)
        n, b, _ = plt.hist( samples[:,i], bins = 50 )
        param_map[i] = b[np.argmax(n)]
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
    
    #%%
    plt.figure(3)
    plt.clf()
    data_map = generate_model(X, Y, xaxis, yaxis, np.append( param_map, np.mean( samples[:,NUM_LENS_PARAM:], axis = 0) )) 
    plt.pcolor(X, Y, data_map)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.title('model from MAP param')
    plt.colorbar()
    plt.show()
    
    #%%
    plt.figure(5)
    plt.clf()
    mean_source = np.mean( samples[:,NUM_LENS_PARAM:], axis = 0).reshape( xaxis.size, yaxis.size)
    plt.pcolor(Xs, Ys, mean_source, vmin = 0, vmax = 6.5 )
    plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
    plt.title('Mean pixelated source profile', fontsize = 20)
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.colorbar()
    
    plt.figure(6)
    plt.clf()
    plt.pcolor(Xs, Ys,S, vmin = 0, vmax = 6.5 )
    plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
    plt.colorbar()
    plt.title('True pixelated source profile', fontsize = 20)
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.show()
    
    plt.figure(10)
    plt.clf()
    plt.pcolor(Xs, Ys,mean_source - S )
    plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
    plt.colorbar()
    plt.title('Error', fontsize = 20)
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.show()
    #%%
    import corner
    
    cplot = corner.corner(samples[:, [0, 1, 2, 3, 4, 5]], bins = 20, 
                  quantiles = [0.16,0.5,  0.84], 
                range = [0.99]*6,
                  show_titles = True, 
                  truths = [re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true],
                    verbose = True, 
                title_kwargs={"fontsize": 20},
                label_kwargs={"fontsize": 20},
        labels=[r"$b$", r"$x_L$",r"$y_L$",r"$e_L$",r"$s_L$",r"$\theta_L$"])
    plt.show()
    
    #%%
    np.save("/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/output/SIEpixel/samples0_2.npy",samples0)
    np.save("/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/output/SIEpixel/samples_2.npy",samples)