# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 21:30:50 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from misc import rotateByTheta
import functools

plt.rcParams['image.cmap'] = 'inferno'

def EllipticalGaussian(X, Y, xSc, ySc, thetaS, aS, qS, s0):
    sx = aS
    sy = aS*qS
    A = np.cos(thetaS)**2/sx**2 + np.sin(thetaS)**2/sy**2
    B = np.sin(thetaS)**2/sx**2 + np.cos(thetaS)**2/sy**2
    C = 2*np.sin(thetaS)*np.cos(thetaS)*(1./sx**2 - 1/sy**2)
    return s0*np.exp( -(X-xSc)**2/2*A - (Y-ySc)**2/2*B - C/2*(X-xSc)*(Y-ySc))
    
def ConstantEllipse(  X, Y, xSc, ySc, thetaS, aS, qS, s0):
    bS = aS*qS
    X0, Y0 = rotateByTheta(X, Y, -thetaS)
    xSc0, ySc0 = rotateByTheta(xSc, ySc, -thetaS)
    S = ( (X0-xSc0)**2/aS**2 + (Y0 - ySc0)**2/bS**2 <= 1  )*s0
    return S
    
def Sersic(X, Y, xSc, ySc, thetaS, aS, qS,  s0, nS ):
    k = 1.9992*nS-0.3271
    # rotate the coords to align with ellipse major axis
    X0, Y0 = rotateByTheta(X, Y, -thetaS)
    xSc0, ySc0 = rotateByTheta(xSc, ySc, -thetaS)
    R = (X0-xSc0)**2+(Y0-ySc0)**2/qS**2
    return s0*np.exp( -k*((R/aS)**(1/nS) - 1))
    
    
if __name__ == '__main__':
    # test constant Ellipse profile
    xSc, ySc = 1.0, 1.5
    theta = 40*np.pi/180
    a = 1.5
    e = 0.4
     
    
    plt.figure(4)
    plt.clf()
    X, Y = np.meshgrid( np.linspace(-1, 3, 100), np.linspace(0, 4, 100))
    f = functools.partial(EllipticalGaussian, xSc = 1.0, ySc=1.5, thetaS=np.pi*20/180, aS=0.6, qS=0.5, s0=1)
    S = f( X, Y )    
    plt.pcolor(X, Y, S)
    plt.colorbar()
    
    
    plt.figure(5)
    plt.clf()
    f =  functools.partial(Sersic, xSc = 1.0, ySc=1.5, thetaS=np.pi*20/180, aS=0.3, qS=0.5, s0=1, nS=4)
    S = f( X, Y )    
    ax1 = plt.pcolor(X, Y, S)
    ax1.set_norm(LogNorm())
    plt.colorbar()
    
    
    plt.figure(6)
    plt.clf()
    X, Y = np.meshgrid( np.linspace(-1, 1, 16), np.linspace(-1, 1, 16))
    S = EllipticalGaussian(X, Y, 0.2, -0.3, 0.2*np.pi, 0.2, 0.6, 2) + EllipticalGaussian(X, Y, -0.35, 0.35, 0.8*np.pi, 0.1, 0.8, 3) 
    ax1 = plt.pcolor(X, Y, S)
    plt.colorbar()
    plt.show()
    
    
