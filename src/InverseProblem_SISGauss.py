# -*- coding: utf-8 -*-
"""
Created on Sun May 15 18:42:00 2016

@author: xguo
"""


import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
from astropy.convolution import convolve, Gaussian2DKernel
import functools
from slicesample import slicesample
from timeit import default_timer as timer
import math

plt.rcParams['image.cmap'] = 'inferno'

gauss_kernel = Gaussian2DKernel(1)


LARGENEG = -1e10

def generate_model(X, Y, params, verbose=False):
    re, xLc, yLc, xSc, ySc, thetaS, aS, qS, s0 = params
    lens  = lenses.SIS([re, xLc, yLc], verbose)
    if (verbose):
        lens.drawCrit()
        lens.drawCaustic()
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=aS, qS=qS, s0=s0)
    model = forward.LensFunction(X, Y, lens, f)
    model = gaussian_filter(model, sigma = 1)
    return model


def logprior_general(param, param_bounds):
    if ( np.any( param<param_bounds[:,0]) or np.any( param>param_bounds[:,1] ) ):
        return LARGENEG
    else:
        return 0
            
def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior( params, X, Y, data, PeakSoverN, logprior):
    model = generate_model(X, Y, params)
    datamax = data.max()
    return loglikelihood(model, data, PeakSoverN) + logprior(params)
    


if __name__ == "__main__":
    #image parameters
    xmin, xmax = -3, 3
    ymin, ymax = -3, 3
    nx, ny = 64, 64
    #generate data
    zL_true = 0.3
    zS_true = 0.7
    sigma_true = 300
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    xSc_true, ySc_true =  0.2, 0.6
    thetaS_true = 10*np.pi/180
    aS_true = 0.4
    qS_true = 0.6
    nS_true = 3.9
    s0_true = 1
    
    re_min = 0
    re_max = min( (ymax - ymin)/2, (xmax - xmin)/2)
    xSc_min, xSc_max, xLc_min, xLc_max = xmin/2, xmax/2, xmin/2, xmax/2
    ySc_min, ySc_max, yLc_min, yLc_max = ymin/2, ymax/2, ymin/2, ymax/2
    thetaS_min, thetaS_max = -math.inf, math.inf
    aS_min, aS_max = 0, min( (ymax - ymin)/4, (xmax - xmin)/4)
    qS_min, qS_max = 0, 1
    s0_min, s0_max = 0, 5
    
    param_bounds = np.array([ [re_min, re_max],  
                             [xLc_min, xLc_max],
                             [yLc_min, yLc_max],
                             [xSc_min, xSc_max],
                             [ySc_min, ySc_max],
                             [thetaS_min, thetaS_max],
                             [aS_min, aS_max],
                             [qS_min, qS_max], 
                             [s0_min, s0_max]
                             ])

    logprior =  functools.partial(logprior_general, param_bounds = param_bounds)
           
    #width 
    re_w = (re_max - re_min)/5
    xLc_w, yLc_w, xSc_w, ySc_w = (xLc_max - xLc_min)/5, (yLc_max - yLc_min)/5, (xSc_max - xSc_min)/5, (ySc_max - ySc_min)/5
    thetaS_w = np.pi/4
    aS_w = (aS_max - aS_min)/5
    qS_w = (qS_max - qS_min)/5
    s0_w = (s0_max - s0_min)/5
    
    
    param_true =  np.array([re_true, xLc_true, yLc_true,   
                  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true] )
    param_w = np.array( [re_w, xLc_w, yLc_w,  
                  xSc_w, ySc_w,  thetaS_w, aS_w, qS_w, s0_w] )
    param_g = param_true + np.random.normal(0, 1)*param_w
    while ( logprior(param_g) < 0):
        param_g = param_true + np.random.normal(0, 1)*param_w
        
    PeakSoverN = 20

    #Image grid
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)  )
    plt.figure(2)
    plt.clf()
    data = generate_model(X, Y, param_true, True)  
    data += forward.add_noise(data, PeakSoverN)
    
    # construct log posterior based on data, grid, and noise level
    logpdf = functools.partial(logposterior, X=X, Y=Y, data=data, PeakSoverN = PeakSoverN, logprior = logprior)    
    
    #show mock data       
    ax1 = plt.pcolor(X,Y, data)
    #ax1.set_norm(LogNorm())
    draw_ellipse(aS_true, aS_true*qS_true, xSc_true, ySc_true, thetaS_true, color = 'r')
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.title('Mock lensing data with S/N={:.0f}'.format(PeakSoverN))
    plt.show()
    # %%
    Nsample = np.int(2e4)
    samples, neval = slicesample(param_true, Nsample, logpdf, thin=1, burnin = int(Nsample/10), width = param_w)    
    # make the angle within 0, pi
    colthetaS = 5
    samples[colthetaS ,:] =  samples[colthetaS ,:]  % np.pi
#%%
    plt.figure(1)
    plt.clf()
    param_map = np.zeros(param_true.shape)
    for i in range(param_true.size): 
        plt.subplot(3,4, i+1)
        n, b, _ = plt.hist( samples[:,i], bins = 50 )
        param_map[i] = b[np.argmax(n)]
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
    
    
    plt.figure(3)
    plt.clf()
    data_map = generate_model(X, Y, param_map)
    plt.pcolor(X, Y, data_map)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.show()
    
    #%%
    import corner
    
    cplot = corner.corner(samples[:, [0, 6, 7, 8]], bins = 25, 
                  quantiles = [0.16,0.5,  0.84], 
                range = [0.9999]*4,
                  show_titles = True, 
                  truths = [re_true, aS_true, qS_true, s0_true],
                    verbose = True, 
                title_kwargs={"fontsize": 20},
                label_kwargs={"fontsize": 20},
        labels=[r"$b$", r"$a_S$", r"$q_S$",
                                     r"$s_0$"])
    plt.show()
