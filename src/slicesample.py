# -*- coding: utf-8 -*-
"""
Created on Sat May 14 15:25:01 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
import sys
plt.rcParams['image.cmap'] = 'inferno'

def f(x):
    return np.exp( -x**2/2)*(1+(np.sin(3*x))**2)*( 1 + (np.cos(5*x))**2)
def logf(x):
    return (-x**2/2)+np.log(1+(np.sin(3*x))**2)+ np.log(  1 + (np.cos(5*x))**2)
    
def log2df(z):
    x, y = z
    return (-(x-1)**2/2 - (y-2)**2/2/0.5**2)+np.log(1+(np.sin(3*x))**2)+ np.log(  1 + (np.cos(5*y))**2)

def slicesample(initial, nsamples, logpdf, thin=1, burnin=0, width = 10, bar_length = 40, plot_real = False, generate_model_part=[]):
    maxiter = 200
    dim = initial.shape[0]
    rnd = np.zeros( (nsamples, dim) )
    neval = nsamples
    e = np.random.exponential(1, (nsamples*thin+burnin, 1))
    RW = np.random.rand(  nsamples*thin+burnin, dim )
    RD = np.random.rand(  nsamples*thin+burnin, dim )
    x0 = initial
    
    if (plot_real):
        plt.figure(5, figsize=(5, 5))
        plt.clf()
        plt.ion()
    
    for i in range(-burnin, nsamples*thin):
        z = logpdf(x0) - e[i+burnin]
        r = width*RW[i+burnin, :]
        xl = x0 - r
        xr = xl + width
        niter = 0
        
        if (dim == 1):
            while ( (logpdf(xl)>z) and niter<maxiter):
                xl = xl - width
                niter += 1
            if ( (niter >=maxiter) or np.any( xl < -np.sqrt(np.finfo(np.float64).max) ) ):
                raise Exception('ErrStepOut')
            neval += niter
            niter = 0
            while ((logpdf(xr)>z) and niter < maxiter):
                xr += width
                niter += 1
            if ( (niter >=maxiter) or np.any( xr > np.sqrt(np.finfo(np.float64).max) ) ):
                raise Exception('ErrStepOut')
        neval += niter
        
        xp = RD[i+burnin, :]*(xr - xl) + xl
        
        niter = 0
        while ( (not logpdf(xp) > z)  and (niter < maxiter) ):
            rshrink = xp>x0;
            xr[rshrink] = xp[rshrink]
            lshrink = np.logical_not(rshrink)
            xl[lshrink] = xp[lshrink]
            xp = np.random.rand(dim)*(xr-xl) + xl
            niter += 1
        if (niter >= maxiter):
            raise Exception('ErrShrinkin')
        x0 = xp[:]
        if (i >= 0 and i%thin == 0):
            rnd[np.int(i/thin),:] = x0[:]
        neval += niter
        
        #show progress bar
        if ( ((i+burnin) % int( (nsamples*thin+burnin)/100) )  == 0):
            percent =  (i+burnin)/(nsamples*thin+burnin)
            hashes = '#' * int((percent * bar_length))
            spaces = ' ' * (bar_length - len(hashes))
            sys.stdout.write("\rSlice Sampler: [{0}] {1}%".format(hashes + spaces, int((percent * 100))))
            sys.stdout.flush()
            
            #plotting
            if (plot_real):
                plt.pcolor(generate_model_part(x0))
                plt.xlabel('x [arcsec]', fontsize = 20)
                plt.ylabel('y [arcsec]', fontsize = 20)
                plt.suptitle('#iter={:06d}'.format(i+burnin), fontsize = 25)
                plt.pause(0.1)
        
    neval = neval*1.0/(nsamples*thin+burnin)
    print('\nDone!')
    return rnd, neval
    
if __name__ == '__main__':
    nsamples = 5000
    thin = 5
    burnin = 1000
    width = 10
    initial = np.array([1., 1.])
    
    
    #%%
    plt.figure(1)
    plt.clf()
    X, Y = np.meshgrid( np.linspace(-2, 3, 100), np.linspace(-1,4, 100))
    Z = np.exp(log2df((X,Y)))
    plt.pcolor(X,Y, Z)
    
    plt.figure(2)
    plt.clf()
    rnd, neval = slicesample(initial, nsamples, log2df,thin, burnin)
    plt.hist2d(rnd[:,0],rnd[:,1], bins=[ X[0,:], Y[:,0]] )
    plt.show()