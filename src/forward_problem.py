# -*- coding: utf-8 -*-
"""
Created on Tue May 10 15:55:27 2016

@author: xguo
"""
import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from scipy.ndimage.filters import gaussian_filter
import functools

plt.rcParams['image.cmap'] = 'inferno'

def LensImage(xgrid, ygrid, lens, sx, sy, sI):
    """
    Assumes sI is a 1D array, that goes through row first from the top
    """
    #deflect the rays
    Xs, Ys, mu = lens.deflect(xgrid, ygrid)    
    
    f = interpolate.RectBivariateSpline(
            sx, sy, sI.reshape(sy.size, sx.size).T 
    )
      
    #find where do the deflected rays deflect      
    Image = f.ev(Xs.ravel(), Ys.ravel())
    #get rid of small numbers that were artifacts of interpolation
    SMALL = 1e-4
    Image[Image<SMALL] =0
    #magnify the sources
    Image = Image*mu.ravel()
    
    return Image.reshape(Xs.shape)

def LensFunction(xgrid, ygrid, lens, f):
    """
    Assumes sI is a 1D array, that goes through row first from the top
    """
    #deflect the rays
    Xs, Ys, mu = lens.deflect(xgrid, ygrid)    
    
    Image = f(Xs.ravel(), Ys.ravel())
    #get rid of small numbers that were artifacts of interpolation
    SMALL = 1e-4
    Image[Image<SMALL] =0
    #magnify the sources
    Image = Image*mu.ravel()
    
    return Image.reshape(Xs.shape)

    
def add_noise(image, PeakSoverN): 
    return np.random.normal(0, 1, image.shape)*image.max()/PeakSoverN

#%%
if __name__ == '__main__':
    #%%
    ## parametrized model testing
    zL = 0.3
    zS = 0.42
    xLc = 0#1.0
    yLc = 0#0.5
    sigma = 600/1.2
    thetaL = 0.2*np.pi 
    ellipL = 0.3
    sL  = 0
    lens  = lenses.SIE([return_re(zL, zS, sigma), xLc, yLc, ellipL, sL, thetaL], True)
    

    xSc, ySc =  0.1, 0.#1.0-0.5,1.0-0.5#1.4, 0.9
    
    sigma1 = 0.2#0.125
    sigma2 = 0.125#0.125
    thetaS = (40+90)*np.pi/180
        
    X, Y = np.meshgrid( np.linspace(-4, 4, 65), np.linspace(-4, 4, 65)  )
    
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=sigma1, qS=sigma2/sigma1, s0=1)
    Image = LensFunction(X, Y, lens, f)
    Image = gaussian_filter(Image, sigma = 1)
    plt.figure(1)
    plt.clf()
    PeakSoverN = 20
    plt.pcolor(X,Y, Image + 1*add_noise(Image, PeakSoverN) )
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()    
    draw_ellipse(sigma1*2, sigma2*2, xSc, ySc, thetaS, 'r')
    lens.drawCrit()
    lens.drawCaustic()
    plt.show()

    #%% 
    #pixelated model testing
    zL = 0.3
    zS = 0.6
    xLc = 0#1.0
    yLc = 0#0.5
    sigma = 275
    thetaL = 0.2*np.pi 
    ellipL = 0.3
    sL  = 0
    lens  = lenses.SIE([return_re(zL, zS, sigma), xLc, yLc, ellipL, sL, thetaL], True)    
    
    X, Y = np.meshgrid( np.linspace(-2.5, 2.5, 64), np.linspace(-2.5, 2.5, 64)  )    
    
    xaxis = np.linspace(-0.8, 0.8, 12)
    yaxis = np.linspace(-0.8, 0.8, 12)
    Xs, Ys = np.meshgrid(xaxis, yaxis)
    xSc1, ySc1 = 0.2, -0.2
    xSc2, ySc2 =-0.2, 0.05
    thetaS1, thetaS2 = 0.3*np.pi, 0.8*np.pi
    aS1, aS2 = 0.2, 0.1
    qS1, qS2 = 0.6, 0.8
    S = srcprf.EllipticalGaussian(Xs, Ys, xSc1, ySc1, thetaS1, aS1, qS1, 5) + srcprf.EllipticalGaussian(Xs, Ys,  xSc2, ySc2, thetaS2, aS2, qS2, 7) 
    plt.figure(3)
    plt.clf()
    plt.pcolor(Xs, Ys, S)
    plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
    plt.colorbar()
    
    
    image = LensImage(X, Y, lens, xaxis, yaxis, S.ravel())
    image = gaussian_filter(image, sigma = 1)
    PeakSoverN = 40
    #image += add_noise(image, PeakSoverN)
    plt.figure(2)
    plt.clf()
    plt.pcolor(X, Y, image)
    lens.drawCrit()
    lens.drawCaustic()
    draw_ellipse(aS1, qS1*aS1, xSc1, ySc1, thetaS1, 'r')
    draw_ellipse(aS2, qS2*aS2, xSc2, ySc2, thetaS2, 'r')
    draw_rect(
        xaxis.min(), yaxis.min(),
        xaxis.max() - xaxis.min(),
        yaxis.max() - yaxis.min()    )
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.show()
