# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:34 2016

@author: xguo
"""



import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
import functools
from slicesample import slicesample
from timeit import default_timer as timer
import math

plt.rcParams['image.cmap'] = 'inferno'


LARGENEG = -1e10

def generate_model(X, Y, params, verbose=False):
    re, xLc, yLc, ellipL, sL, thetaL, xSc, ySc,  thetaS, aS, qS, s0 = params
    lens  = lenses.SIE([re, xLc, yLc, ellipL, sL, thetaL], verbose)
    if (verbose):
        lens.drawCrit()
        lens.drawCaustic()
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=aS, qS=qS, s0=s0)
    model = forward.LensFunction(X, Y, lens, f)
    model = gaussian_filter(model, sigma = 1)
    return model

def logprior_general(param, param_bounds):
    if ( np.any( param<param_bounds[:,0]) or np.any( param>param_bounds[:,1] ) ):
        return LARGENEG
    else:
        return 0
            
def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior( params, X, Y, data, PeakSoverN, logprior):
    model = generate_model(X, Y, params)
    return loglikelihood(model, data, PeakSoverN) + logprior(params)
    


if __name__ == "__main__":
    #image parameters
    xmin, xmax = -4, 4
    ymin, ymax = -4, 4
    nx, ny = 64, 64
    #generate data
    zL_true = 0.3
    zS_true = 0.72
    sigma_true = 350
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    thetaL_true = 0.2*np.pi 
    ellipL_true = 0.3
    sL_true  = 0.1
    
    xSc_true, ySc_true =  0.1, 0
    thetaS_true = 130*np.pi/180
    aS_true = 0.2
    qS_true = 0.125/0.2
    s0_true = 1
    
    re_min = 0
    re_max = min( (ymax - ymin)/2, (xmax - xmin)/2)
    ellipL_min, ellipL_max = 0, 1
    sL_min, sL_max = 0, 1
    thetaL_min, thetaL_max = -math.inf, math.inf
    xSc_min, xSc_max, xLc_min, xLc_max = xmin/2, xmax/2, xmin/2, xmax/2
    ySc_min, ySc_max, yLc_min, yLc_max = ymin/2, ymax/2, ymin/2, ymax/2
    thetaS_min, thetaS_max = -math.inf, math.inf
    aS_min, aS_max = 0, min( (ymax - ymin)/4, (xmax - xmin)/4)
    qS_min, qS_max = 0, 1
    s0_min, s0_max = 0, 5
    
    param_bounds = np.array([ [re_min, re_max],  
                             [xLc_min, xLc_max],
                             [yLc_min, yLc_max],
                             [ellipL_min, ellipL_max], 
                             [sL_min, sL_max],
                             [thetaL_min, thetaL_max],
                             [xSc_min, xSc_max],
                             [ySc_min, ySc_max],
                             [thetaS_min, thetaS_max],
                             [aS_min, aS_max],
                             [qS_min, qS_max], 
                             [s0_min, s0_max]
                             ])

    logprior =  functools.partial(logprior_general, param_bounds = param_bounds)
           
    #width 
    re_w = (re_max - re_min)/5
    xLc_w, yLc_w, xSc_w, ySc_w = (xLc_max - xLc_min)/5, (yLc_max - yLc_min)/5, (xSc_max - xSc_min)/5, (ySc_max - ySc_min)/5
    ellipL_w = (ellipL_max - ellipL_min)/5
    sL_w = (sL_max - sL_min)/10
    thetaL_w = np.pi/4
    thetaS_w = np.pi/4
    aS_w = (aS_max - aS_min)/5
    qS_w = (qS_max - qS_min)/5
    s0_w = (s0_max - s0_min)/5
    
    
    param_true =  np.array([re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true, 
                  xSc_true, ySc_true,  thetaS_true, aS_true, qS_true, s0_true] )
    param_w = np.array( [re_w, xLc_w, yLc_w,  ellipL_w, sL_w, thetaL_w, 
                  xSc_w, ySc_w,  thetaS_w, aS_w, qS_w, s0_w] )
    #param_g = np.array( [2.2, 0, 0, 0.2, 0.0, 0, 0, 0, 0, 0.8, 0.5, 1] )
    param_g = param_true + np.random.normal(0, 1)*param_w
    while ( logprior(param_g) == LARGENEG):
        param_g = param_true + np.random.normal(0, 1)*param_w/2
    param_g[0] = 2.15
    param_g[3] = 0.2
    param_g[4] = 0.1
    param_g[9] = 0.1
    param_g[10] = 0.5
    param_g[11] = 2
    
    # %%
    PeakSoverN = 20

    #Image grid
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)  )
    #generate mock data
    data = generate_model(X, Y, param_true, True)  
    data += forward.add_noise(data, PeakSoverN)
    
    # construct log posterior based on data, grid, and noise level
    logpdf = functools.partial(logposterior, X=X, Y=Y, data=data, PeakSoverN = PeakSoverN, logprior = logprior)    
    
    #show mock data
    plt.figure(1)
    plt.clf()       
    plt.pcolor(X,Y, data)
    #draw_ellipse(aS_true, aS_true*qS_true, xSc_true, ySc_true, thetaS_true, color = 'r')
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.title('lensed and blurred data with S/N={:.0f}'.format(PeakSoverN), fontsize = 16)
    plt.savefig('output/SIEGauss/data3.png')
    
    plt.figure(2)
    plt.clf()       
    guess_model = generate_model(X, Y, param_g, False)  
    plt.pcolor(X, Y, guess_model)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.title('initial guess'.format(PeakSoverN), fontsize = 16)
    plt.savefig('output/SIEGauss/initial_guess3.png')
    # %%
    # perform slice sampling
    Nsample0 = np.int(6e4)
    burnin = int(Nsample0/10)
    thin = 2
    Nsample = Nsample0*thin + burnin
    
    param_toplot = [0, 3, 4,  9, 10, 11]
    labels=[r"$b$", r"$e_L$",r"$s_L$",
        r"$a_S$", r"$q_S$", r"$s_0$"]
    samples0, neval = slicesample(param_g, Nsample, logpdf, thin=1, burnin =0, width = param_w, plot_real = False)   
    samples = samples0[burnin::thin,:]
    # make the angle within 0, pi
    colthetaS = 8
    samples[colthetaS ,:] =  samples[colthetaS ,:]  % np.pi
    colthetaL = 5
    samples[colthetaL ,:] =  samples[colthetaL ,:]  % np.pi
    #% plot rough histogram of posterior and find the MAP of each marginal distribution
    plt.figure(3)
    plt.clf()
    param_map = np.zeros(param_true.shape)
    for i in range(param_true.size): 
        plt.subplot(3,4, i+1)
        n, b, _ = plt.hist( samples[:,i], bins = 50 )
        param_map[i] = b[np.argmax(n)]
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
    
    #visualize the model produced by MAP parameter
    plt.figure(4)
    plt.clf()
    data_map = generate_model(X, Y, param_map)
    plt.pcolor(X, Y, data_map)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.title('image from MAP param')
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.show()
    
    #%
    # Make prettier plot
    import corner
    cplot = corner.corner(samples[:, [0, 3, 4,  9, 10, 11]], bins = 25, 
                  quantiles = [0.16,0.5,  0.84], 
                range = [0.9999]*6,
                  show_titles = True, 
                  truths = [re_true, ellipL_true, sL_true, 
                            aS_true,   qS_true, s0_true],
                    verbose = True,
                title_kwargs={"fontsize": 20},
                label_kwargs={"fontsize": 20},
        labels=[r"$b$", r"$e_L$",r"$s_L$",
        r"$a_S$", r"$q_S$", r"$s_0$"])
    plt.show()
    
    #%%
    #np.save("/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/SIEGauss_sample0.npy",samples0)
    #np.save("/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/SIEGauss_sample.npy",samples)