# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:34 2016

@author: xguo
"""

import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm
from scipy.ndimage.filters import gaussian_filter
from astropy.convolution import convolve, Gaussian2DKernel
import functools

plt.rcParams['image.cmap'] = 'inferno'

gauss_kernel = Gaussian2DKernel(2)

re_min = 0.5
re_max = 5
xLc_mean, yLc_mean = 0, 0
xLc_min, xLc_max = 0, 1
yLc_min, yLc_max = 0, 1
xSc_mean, ySc_mean = 0, 0
xLc_sigma, yLc_sigma, xSc_sigma, ySc_sigma = 1, 1, 1, 1
sx_min, sx_max = 0, 3
sy_min, sy_max = 0, 3
xSc_min, xSc_max = -2, 2
ySc_min, ySc_max = -2, 2
s0_min, s0_max = 0, 10
LARGENEG = -1e10

def generate_model(X, Y, params):
    re, xLc, yLc, ellipL, sL, thetaL, xSc, ySc, thetaS, sx, sy, s0 = params
    lens  = lenses.SIE([re, xLc, yLc, ellipL, sL, thetaL], False)
    f = functools.partial(srcprf.EllipticalGaussian, x0 = xSc, y0=ySc, theta=thetaS, sx=sx, sy=sy, s0=s0)
    return forward.LensFunction(X, Y, lens, f)

def logprior(params, datamax):
    re, xLc, yLc, ellipL, sL, thetaL, xSc, ySc, thetaS, sx, sy, s0 = params
    if (sx < sx_min or sx> sx_max or sy< sy_min or sy> sy_max or s0 < s0_min or s0 > s0_max or re<re_min or re>re_max or 
        xSc < xSc_min or ySc < ySc_min or xSc > xSc_max or ySc > ySc_max
        or xLc<xLc_min or xLc > xLc_max
        or yLc<yLc_min or yLc > yLc_max):
        return LARGENEG
    else:
        return 0#np.sum( -( np.array([xLc, yLc, xSc, ySc]) - np.array([xLc_mean, yLc_mean, xSc_mean, ySc_mean]) )**2/(
            #np.array([xLc_sigma, xLc_sigma, xSc_sigma, ySc_sigma])**2/2) )

def loglikelihood(model, data, PeakSoverN):
    sigmaNoise = data.max()/PeakSoverN
    return -1./2*np.sum( (model - data)**2 )/sigmaNoise**2
    

def logposterior(X, Y, data, params, PeakSoverN):
    model = generate_model(X, Y, params)
    datamax = data.max()
    return loglikelihood(model, data, PeakSoverN) + logprior(params, datamax)
    
def proposal(param, propstep):
    return param+np.random.normal(0, 1, param.shape)*propstep

if __name__ == "__main__":
    
    #generate data
    zL_true = 0.3
    zS_true = 0.42
    sigma_true = 600/1.2
    
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    thetaL_true = 0.2*np.pi 
    ellipL_true = 0.3
    sL_true  = 0
    xSc_true, ySc_true =  0.0, 0
    sigma1_true = 0.125
    sigma2_true = 0.2
    thetaS_true = 40*np.pi/180
    s0_true = 2
    param_true =  np.array([re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true,  
                  xSc_true, ySc_true,  thetaS_true, sigma1_true, sigma2_true, s0_true] )
    PeakSoverN = 50
    #Image grid
    X, Y = np.meshgrid( np.linspace(-5, 5, 64), np.linspace(-5, 5, 64)  )
    
    data = generate_model(X, Y, param_true)     
    #data = convolve(data, gauss_kernel)
    data += forward.add_noise(data, PeakSoverN)
    
    
    plt.figure(4)
    plt.clf()   
    plt.pcolor(X,Y, data)
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.colorbar()
    plt.title('Lensed and Blurred Mock data')
    plt.show()
    # %%
    #initial guess
    re_g = 2.
    xLc_g, yLc_g = 0,0
    ellipL_g = 0.1
    sL_g = 0
    thetaL_g = 0 
    xSc_g, ySc_g = 0,0
    thetaS_g = 0
    sigma1_g, sigma2_g = 0.3, 0.3
    s0_g = 2
    
    param_g = np.array( [re_g, xLc_g, yLc_g, ellipL_g, sL_g, thetaL_g,  
                  xSc_g, ySc_g,  thetaS_g, sigma1_g, sigma2_g, s0_g] )
    
                  
    Nsample = np.int(1e4)
    samples = np.zeros( (param_true.size, Nsample))
    
    re_step = 0.1
    xLc_step = 0.1
    yLc_step = 0.1
    ellipL_step = 0.05
    sL_step = 0.1
    thetaL_step = np.pi/10
    xSc_step = 0.1
    ySc_step = 0.1
    thetaS_step = np.pi/10
    sigma1_step = 0.05
    sigma2_step = 0.05
    s0_step = 0.05
    
    
    prop_step = np.array([re_step, xLc_step, yLc_step, ellipL_step, sL_step, thetaL_step,  
                  xSc_step, ySc_step,  thetaS_step, sigma1_step, sigma2_step, s0_step] )
    param_g = proposal(param_true, prop_step)
    
    Nacc = 0
    Niter = 0
    ind = 0
    x_old = param_g
    while (Nacc < Nsample):        
        p_old = logposterior(X, Y, data, x_old, PeakSoverN)
        x_new = proposal(x_old, prop_step)
        p_new = logposterior(X, Y, data, x_new, PeakSoverN)
        alpha = np.exp( p_new - p_old)
        
        if (alpha > 1):
            samples[:, Nacc] = x_new[:]
            x_old = x_new
            Nacc += 1
        else:
            u = np.random.rand()
            if (u < alpha):
                samples[ :, Nacc] = x_new[:]
                x_old = x_new
                Nacc += 1
        Niter +=1
        if (Niter % 1000 == 0):
            print("Niter {:} Nacc {:} alpha{:}".format(Niter, Nacc, alpha))
            
    colthetaL = 5
    colthetaS = 8
    samples[colthetaL ,:] =  samples[colthetaL ,:]  % np.pi
    samples[colthetaS ,:] =  samples[colthetaS ,:]  % np.pi
    #%%
    plt.figure(1)
    plt.clf()
    burnin = 0#np.int(0.2*Nsample)
    for i in range(param_true.size): 
        plt.subplot(3,4, i+1)
        plt.hist( samples[i,burnin:Nacc], bins = 10 )
        plt.axvline( param_true[i], color = 'r')
        plt.axvline( param_g[i], color = 'y', linestyle = '--')
    plt.show()
