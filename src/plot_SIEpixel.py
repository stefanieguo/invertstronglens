import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
import forward_problem as forward
from misc import draw_ellipse, draw_rect, return_re
from scipy import interpolate
from matplotlib.colors import LogNorm, PowerNorm, SymLogNorm
from scipy.ndimage.filters import gaussian_filter
import functools
from slicesample import slicesample
from InverseProblem_SIEpixel import generate_model
from timeit import default_timer as timer
import math

plt.rcParams['image.cmap'] = 'inferno'

if __name__ == "__main__":
    fname = "/media/storage/Dropbox/Classes/AM216/project/code/InverseLensing/output/SIEpixel/samples0_2.npy"
    samples = np.load(fname)

    xmin, xmax = -2.5, 2.5
    ymin, ymax = -2.5, 2.5
    nx, ny = 64, 64
    #generate data
    zL_true = 0.3
    zS_true = 0.6
    sigma_true = 275
    re_true = return_re(zL_true, zS_true, sigma_true)
    xLc_true, yLc_true = 0, 0
    thetaL_true = 0.2*np.pi 
    ellipL_true = 0.3
    sL_true  = 0.05
    
    
    re_min, re_max = 0, min( (ymax - ymin)/2, (xmax - xmin)/2)
    ellipL_min, ellipL_max = 0, 1
    sL_min, sL_max = 0, 0.2
    thetaL_min, thetaL_max = -math.inf, math.inf
    xLc_min, xLc_max = xmin/2, xmax/2
    yLc_min, yLc_max = ymin/2, ymax/2
    
    PeakSoverN = 50
    
    xaxis = np.linspace(-0.8, 0.8, 12)
    yaxis = np.linspace(-0.8, 0.8, 12)
    Xs, Ys = np.meshgrid(xaxis, yaxis)
    xSc1, ySc1 = 0.2, -0.2
    xSc2, ySc2 =-0.2, 0.05
    thetaS1, thetaS2 = 0.3*np.pi, 0.8*np.pi
    aS1, aS2 = 0.2, 0.1
    qS1, qS2 = 0.6, 0.8
    S = srcprf.EllipticalGaussian(Xs, Ys, xSc1, ySc1, thetaS1, aS1, qS1, 5) + srcprf.EllipticalGaussian(Xs, Ys,  xSc2, ySc2, thetaS2, aS2, qS2, 7) 
    
    
    param_bounds = np.array([ [re_min, re_max],  
                             [xLc_min, xLc_max],
                             [yLc_min, yLc_max],
                             [ellipL_min, ellipL_max], 
                             [sL_min, sL_max],
                             [thetaL_min, thetaL_max]
                             ])
                             
    param_true =  np.append( np.array([re_true, xLc_true, yLc_true, ellipL_true, sL_true, thetaL_true] ), S.ravel() ) 
    
    #Image grid
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)  )
    #generate_data
    data = generate_model(X, Y, xaxis, yaxis, param_true, True)  
    data += forward.add_noise(data, PeakSoverN)
    
    
    re_w = (re_max - re_min)/5
    xLc_w, yLc_w = (xLc_max - xLc_min)/5, (yLc_max - yLc_min)/5
    ellipL_w = (ellipL_max - ellipL_min)/5
    sL_w = (sL_max - sL_min)/2
    thetaL_w = np.pi/4
    
    param_w = np.append( np.array( [re_w, xLc_w, yLc_w,  ellipL_w, sL_w, thetaL_w] )*2, np.ones(S.ravel().shape)*5 ) 
    
    
    param_toplot = [0, 1, 2, 3, 4, 5]
    labels=[r"$b$", r"$x_L$",r"$y_L$",r"$e_L$",r"$s_L$",r"$\theta_L$"]
    col = len(param_toplot)
    
    K = len(param_toplot)
    factor = 2.0           # size of one side of one panel
    lbdim = 0.5 * factor   # size of left/bottom margin
    trdim = 0.2 * factor   # size of top/right margin
    whspace = 0.05         # w/hspace size
    plotdim = factor * K + factor * (K - 1.) * whspace
    dim = lbdim + plotdim + trdim    
    
    plt.figure(10, figsize = (dim, dim))
    plt.clf()
    plt.ion()
    plt.show()
    
    import matplotlib.gridspec as gridspec
    gs = gridspec.GridSpec(K, K)
    
                
    for i, parami in enumerate(param_toplot[:-1]):
        for j, paramj in enumerate(param_toplot[:]):
            if (j>i):
                ax = plt.subplot(gs[j,i])
                plt.plot(param_true[parami],param_true[paramj], 'r*', markersize = 15)
                ax.set_xlim( [param_true[parami] - 0.2*param_w[parami], param_true[parami] + 0.2*param_w[parami]])
                ax.set_ylim( [param_true[paramj] - 0.2*param_w[paramj], param_true[paramj] + 0.2*param_w[paramj]] )
                if (j<K-1):
                    ax.set_xticklabels([])
                    
                else:
                    ax.set_xlabel(labels[i], fontsize = 20)
                    ax.tick_params(labelsize = 10)
                if (i>0):
                    ax.set_yticklabels([])
                else:
                    ax.set_ylabel(labels[j], fontsize = 20)
                    ax.tick_params(labelsize = 10)
    #%%            
    
    
    Nsample0 = np.int(9e4)
    burnin = np.int(Nsample0/5)
    thin = 1
    Nsample = Nsample0*thin + burnin
    
    count = 0
    step = 100
    for dataind in range(0, 2000, 50):
        for i, parami in enumerate(param_toplot[:-1]):
            for j, paramj in enumerate(param_toplot[:]):
                if (j>i):
                    #plt.subplot(K, K,j*col+i+1)
                    plt.subplot(gs[j,i])
                    plt.plot(samples[:dataind, parami], samples[:dataind, paramj], color='b', linewidth = 1)
            
        ax4=plt.subplot(gs[0:3,3:])
        plt.pcolor(X, Y, generate_model(X, Y, xaxis, yaxis, samples[dataind,:]))
        plt.axis([X.min(), X.max(), Y.min(), Y.max()])
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        ax4.tick_params(labelsize = 10)
        plt.title('Image', fontsize = 20)
        
        ax3 = plt.subplot(gs[0:2,1:3])
        plt.pcolor(Xs, Ys, samples[dataind,6:].reshape(xaxis.size, yaxis.size))
        plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
        plt.title('Source', fontsize = 20)      
        ax3.tick_params(labelsize = 10)
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        
        
        plt.suptitle('#iter={:06d}'.format(dataind), fontsize = 25)
        #plt.pause(0.1)
        plt.savefig('anim/SIEPixel/image{:06d}'.format(count))
        count +=1
        print('fine stage', count)
        
    for dataind in range(2000, 10000, 500):
        for i, parami in enumerate(param_toplot[:-1]):
            for j, paramj in enumerate(param_toplot[:]):
                if (j>i):
                    #plt.subplot(K, K,j*col+i+1)
                    plt.subplot(gs[j,i])
                    plt.plot(samples[:dataind, parami], samples[:dataind, paramj], color='b', linewidth = 1)
            
        ax4=plt.subplot(gs[0:3,3:])
        plt.pcolor(X, Y, generate_model(X, Y, xaxis, yaxis, samples[dataind,:]))
        plt.axis([X.min(), X.max(), Y.min(), Y.max()])
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        ax4.tick_params(labelsize = 10)
        plt.title('Image', fontsize = 20)
        
        ax3 = plt.subplot(gs[0:2,1:3])
        plt.pcolor(Xs, Ys, samples[dataind,6:].reshape(xaxis.size, yaxis.size))
        plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
        plt.title('Source', fontsize = 20)      
        ax3.tick_params(labelsize = 10)
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        
        
        plt.suptitle('#iter={:06d}'.format(dataind), fontsize = 25)
        #plt.pause(0.1)
        plt.savefig('anim/SIEPixel/image{:06d}'.format(count))        
        #plt.savefig('anim/SIEGauss/image{:06d}'.format(count))
        count +=1
        print('fine stage', count)
        
    #%%  
    step = 3000
    for dataind in range(10000,  Nsample ,3000):
        for i, parami in enumerate(param_toplot[:-1]):
            for j, paramj in enumerate(param_toplot[:]):
                if (j>i):
                    plt.subplot(gs[j,i])
                    plt.plot(samples[:dataind, parami], samples[:dataind, paramj], color='b', linewidth = 1)
                
            
        ax4=plt.subplot(gs[0:3,3:])
        plt.pcolor(X, Y, generate_model(X, Y, xaxis, yaxis, samples[dataind,:]))
        plt.axis([X.min(), X.max(), Y.min(), Y.max()])
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        ax4.tick_params(labelsize = 10)
        plt.title('Image', fontsize = 20)
        
        ax3 = plt.subplot(gs[0:2,1:3])
        plt.pcolor(Xs, Ys, samples[dataind,6:].reshape(xaxis.size, yaxis.size))
        plt.axis([Xs.min(), Xs.max(), Ys.min(), Ys.max()])
        plt.title('Source', fontsize = 20)      
        ax3.tick_params(labelsize = 10)
        #plt.xlabel('x [arcsec]', fontsize = 10)
        #plt.ylabel('y [arcsec]', fontsize = 10)
        
        plt.suptitle('#iter={:06d}'.format(dataind), fontsize = 25)
        #plt.pause(0.1)
        plt.savefig('anim/SIEPixel/image{:06d}'.format(count))
        #plt.savefig('anim/SIEGauss/image{:06d}'.format(count))
        count +=1
        print('coarse stage', count)
        