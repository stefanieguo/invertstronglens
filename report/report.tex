%% LyX 2.1.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[preprint]{aastex6}
\usepackage[utf8]{inputenc}
\setcounter{tocdepth}{3}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{esint}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.

\usepackage{verbatim}
\usepackage{float}
%\usepackage[backref,breaklinks,colorlinks,citecolor=blue]{hyperref}



\providecommand{\tabularnewline}{\\}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
%\documentclass[letterpaper,10pt]{article}
\usepackage{amsfonts}
%\renewcommand{\vec}{\boldsymbol}



\usepackage{listings}
\renewcommand{\lstlistingname}{\inputencoding{latin9}Listing}

\makeatother

\begin{document}

\author{Xinyi Guo}


\email{xguo@fas.harvard.edu}


\title{APMTH216 Final Project\\
 Inverting Strong Gravitational Lenses}
\begin{abstract}
Strong gravitational lensing happens 
when the gravitational field of massive objects such as galaxy ans galaxy clusters
bends the light of a distant object and produce multiple resolved images. 
Recovering the mass potential of the lens and 
the intrinsic source brightness profile can tell us about the physical
properties of the lens and source, which might be otherwise 
hard to probe. 
In this project, we describe how to generate strong lensing 
images using the ray tracing technique to solve the forward problem. 
We then use a Bayesian framework to infer the lens and source parameters 
from noisy data. We applied the technique to 2 sets of mock data 
and show that our technique successfully recover 
model parameters well within 2-$\sigma$ interval.
\end{abstract}

\section{Introduction}

Einstein’s theory of general relativity states that both massive particles
and massless photons travel along geodesics in spacetime. Thus the
trajectories of distant photons will be deflected by matter along
their way as they travel towards observers. In the case that the foreground
deflectors are massive objects, such as galaxies, galaxy groups or
clusters of galaxies, the deflection is large and often create multiple
images that can be easily resolved by modern telescopes. This phenomenon
is called gravitational strong lensing.

Strong lensing is a powerful tool for at least two reasons. First
of all, the features of the lensed image depends sensitively on the
mass distribution of the lens. Thus it allows us to probe the spatial
distribution of both dark matter and baryon of large scale structures.
Secondly, strong lensing often magnify the background objects by orders
of magnitude. As a result, distant objects that are otherwise too
faint to be observed directly can be revealed if they happen to be
strongly lensed. Inferring the lens mass distribution and the source
brightness profile are the two main tasks of inverting strong gravitational
lenses.

In Section \ref{sec:forward}, we review the basic theory of gravitational
lensing, explicitly show the lensing effects from a singular isothermal
ellipsoid lensing potential, and describe the ray tracing method to simulate
the lensing image.
In Section \ref{sec:inverse}, we describe the Bayesian 
framework to invert the lensing potential and source profile. We 
then applied the technique to 2 sets of mock observational data. 


\section{Forward Problem}

\label{sec:forward}


\subsection{Gravitational Lensing Basics}

\label{sec:lensing}

The basic lensing geometry is shown in Figure \ref{fig:lensgeom}.
For a source at $D_{s}$, lens at $D_{L}$, we denote the separation
between them as $D_{LS}$. %\begin{comment}%$D$ are angular diameter distances%\begin{equation}%D_{A}(z)=\frac{c}{H_{0}}\frac{1}{1+z}\int_{0}^{z}\frac{1}{\sqrt{\Omega_{m}(1+z')^{3}+\Omega_{\Lambda}}}dz'%\end{equation}%\begin{equation}%D_{LS}=\frac{c}{H_{0}}\frac{1+z_{S}}{1+z_{L}}\int_{z_{L}}^{z_{S}}\frac{1}{\sqrt{\Omega_{m}(1+z')^{3}+\Omega_{\Lambda}}}dz'%\end{equation}%we adopt standard cosmology parameters $H_{0}=$, $\Omega_{M}=0.286$,%$\Omega_{\Lambda}=0.714$%\end{comment}We
can relate the two-dimensional \textit{observed} coordinates associated
with a particular light ray ($\vec{\theta}$ in the image plane) to
the two-dimensional coordinates in the source plane ($\vec{\beta}$)
\citep[e.g.][]{1964MNRAS.128..307R} by the lens equation 
\begin{equation}
\vec{\beta}=\vec{\theta}-\vec{\alpha}(\vec{\theta})\label{eq:lens}
\end{equation}
where the reduced deflection angle $\vec{\alpha}$ is related to the
true deflection angle $\hat{\alpha}$ by 
\begin{equation}
\vec{\alpha}=\frac{D_{LS}}{D_{S}}\hat{\alpha}.
\end{equation}


For all astrophysical applications, where the deflection angle is
small, we can apply the weak field limit and relate the deflection
angle to the two-dimensional lensing potential $\psi$ by 
\begin{equation}
\hat{\alpha}=\nabla\psi,
\end{equation}
which in turn satisfies the two-dimensional Poisson equation 
\begin{equation}
\nabla^{2}\psi=2\kappa.
\end{equation}
Here, $\kappa$ is the surface mass density in units of the\textit{
critical density} $\Sigma_{c}=\frac{c^{2}D_{S}}{4\pi GD_{L}D_{LS}}.$
The Jacobian of the transformation from the image to the source plane
gives the inverse magnification matrix 
\begin{equation}
\mu_{ij}^{-1}\equiv\frac{\partial\vec{\beta}}{\partial\vec{\theta}}.
\end{equation}
The magnification factor is given as the absolute value of the determinant
of the magnification matrix 
\begin{equation}
\mu=\frac{1}{|\det\mu_{ij}^{-1}|}.
\end{equation}
When the determinant of the inverse magnification matrix becomes zero,
the magnification becomes formally infinite. The iso-contours in the
\textit{image} plane where the determinant of the inverse magnification
matrix diverges are called the \textit{critical line}s. The corresponding
positions in the \textit{source} plane are called the caustics. When
a source is located near a caustic, it can be magnified by very large
factors.

\begin{figure}
\centering{}\includegraphics[width=8cm]{lensingGeo} \caption{Basic gravitational lensing geometry. A light ray from the source
$S$ with true angle position $\beta$ (located at $D_{S}$ from the
observer) approaching the lens (located at $D_{L}$ from the observer)
at impact parameter $\xi$, is deflected by angle $\hat{\alpha}$.
The deflected the ray reaches the observer, who sees the image $I$
at angle $\theta$.}
\label{fig:lensgeom} 
\end{figure}


%XXX rephrase! It is convenient to define the Einstein radius. For%a circular deflector it is the radius of the region inside where the%average surface-mass density equals the critical density. A point%source perfectly aligned with the center of a circular mass distribution%is lensed into a circle of radius equal to the Einstein radius, the%so-called Einstein ring. The size of the Einstein radius depends on%the enclosed mass as well as on the redshifts of deflector and source. %XXX say something about the Einstein radius is the effective cross%section of lensing. 


\subsection{Singular Isothermal Ellipsoid Lensing potential}

For all purposes in this report, we use the singular isothermal ellipsoid
(SIE) \citep{KormannR.SchneiderP.Bartelmann1994} model as the lensing
potential. It is widely used in research in astronomy and empirically
verified as a good approximation to the combined mass potential of
dark matter and baryon at large scales such as galaxy and galaxy clusters
\citep{Treu2010}. The lensing profile has 3 physical paramters: the
effective Einstein radius $b\equiv4\pi\left(\frac{\sigma_{v}}{c}\right)^{2}\frac{D_{LS}}{D_{S}}$
which is related to the velocity dispersion $\sigma_{v}$ that characterized
the total enclosed mass of the lensing object; the aspect ratio of
the minor axis to the major axis $q_{L}$, which is related to the
ellipticity $e_{L}=1-q_{L}$; and the core radius $s_{L}.$ The lensing
potential takes the form 
\begin{equation}
\psi=\vec{\theta}\cdot\vec{\alpha}-bs_{L}\ln\left[\left(\omega+s_{L}\right)^{2}+\left(1-q_{L}\right)^{2}x'^{2}\right]^{1/2}
\end{equation}
where 
\begin{equation}
\omega^{2}=q_{L}^{2}\left(s_{L}^{2}+x'^{2}\right)+y'^{2}
\end{equation}
and $x',y'$ are coordinates centered on the center of the lensing
potential $\vec{x}_{L}=(x_{L},y_{L})$ and aligned with its principal
axis, which is rotated by angle $\theta_{L}$ from the $x$-axis of
the lens frame. Thus, there are 3 more positional parameters of the
lens. The deflection from the SIE potential takes the form 
\begin{equation}
\alpha_{x}=\frac{b}{\sqrt{1-q_{L}^{2}}}\tan^{-1}\left(\frac{x'\sqrt{1-q_{L}^{2}}}{\omega+s_{L}}\right)\label{eq:ax}
\end{equation}
\begin{equation}
\alpha_{y}=\frac{b}{\sqrt{1-q_{L}^{2}}}\tanh^{-1}\left(\frac{y'\sqrt{1-q_{L}^{2}}}{\omega+q_{L}^{2}s}\right)\label{eq:ay}
\end{equation}
with inverse magnification magnification 
\begin{equation}
\mu^{-1}=\left|1-\frac{b}{\omega}-\frac{b^{2}s_{L}}{\omega\left[\left(\omega+s_{L}\right)^{2}+\left(1-q_{L}^{2}\right)x'^{2}\right]}\right|.\label{eq:mag}
\end{equation}



\subsection{Source Brightness Profile}

In this project, we will test two source brightness profile scenarios.
To start with, we use a \textit{parameterized} elliptical Gaussian
source brightness profile 
\begin{equation}
S(\vec{\beta}')=S_{0}\exp\left[-\frac{\beta_{x}'^{2}}{2a_{S}^{2}}-\frac{\beta_{y}'^{2}}{2a_{S}^{2}q_{S}^{2}}\right]
\end{equation}
where $\vec{\beta}'$ is coordinates centered on the source center
$\vec{x}_{S}=(x_{S},y_{S})$ and aligned with its principal axis,
which is rotated by $\theta_{S}$ from the x-axis of the source frame.
In addition, the 3 physical parameters of the source are the brightness
normalization $S_{0}$, length of the major axis $a_{S}$ and the
aspect ratio $q_{S}$. We will also test pixelated source profile
where $S(\vec{\beta})$ takes free form values.


\subsection{Ray Tracing}

In order to simulate strong lensing images, we use the standard ray
tracing technique \citep[e.g.][]{Newbury2002}, which is described
as follows.

(1) Given an image grid, we shoot rays through the center of each
grid at position $\vec{\theta}$ and initialize the pixel value $I(\vec{\theta}_{j}).$
(2) We calculate the deflection angle of each ray $\vec{\alpha}(\vec{\theta}_{j})$
and solve the lens equation Eq.\ref{eq:lens} for the point $\vec{\beta_{j}}$
where the ray pierces the source plane, given the deflection angles
given by Eqs. \ref{eq:ax}, \ref{eq:ay}. (3) If the deflected ray
$\vec{\beta}_{j}$ resides within the extent of the source, we register
the value at the pixel as $I(\vec{\theta}_{j})=S(\vec{\beta_{j}})\cdot\mu(\vec{\xi}_{j})$
where $\mu(\vec{\xi_{j}})$ is the magnification value at position
$\vec{\xi}_{j}$ in the lens plane given by Eq. \ref{eq:mag}. If
$\vec{\beta}_{j}$ resides beyond the source, then $I(\vec{\theta}_{j})=0$.

In order to find $S(\vec{\beta_{j}})$, we use analytic function when
the source is parameterized. When the source takes free-form pixelated
values, we use bi-linear interpolation to inquire $S(\vec{\beta_{j}})$.

In Figure \ref{fig:demo} we showcase a few example lensing images
produced by the ray tracing technique. All images are generated by
the same SIE potential with $b=2.92''$, $\vec{x}_{L}=(0,0)$, $\theta_{L}=0.2\pi$,
$e_{L}=0.3$, $s_{L}=0''$, whose critical line is shown as the red
dotted ellipse. The elliptical Gaussian source, outlined by the yellow
ellipse, have the same physical parameters $a_{S}=0.16'',q_{S}=0.6,s_{0}=1$,
with only varying positional parameters. The red diamond shaped curve
represents the caustics in the source plane (bounded by the cyan square).

\begin{figure}[h]
\begin{centering}
\includegraphics[width=7cm]{demo_2img}\includegraphics[width=7cm]{demo_cross}
\includegraphics[width=7cm]{demo_cusp}\includegraphics[width=7cm]{demo_fold} 
\par\end{centering}

\caption{Example lensing images generated by ray tracing of an ellipitcal gaussian
source lensed by a SIE potential. The SIE potential have the same
parameter set $b=2.92''$, $\vec{x}_{L}=(0,0)$, $\theta_{L}=0.2\pi$,
$e_{L}=0.3$, $s_{L}=0''$. The critical lines of the lensing potential
is indicated by the red dash dotted line. The source has the same
physical parameter $a_{S}=0.16'',q_{S}=0.6,s_{0}=1$ but have varying
position and angles, which are indicated by the yellow ellipse inside
the cyan box. The red diamond shows the position of the caustics in
the source plane. }
\label{fig:demo} 
\end{figure}



\subsection{Modeling Observations}

In order to mimic real astronomical observations, we first convolve
the image with a Gaussian filter with $\sigma=1$ to represent the
telescope point spread function and then add Gaussian noise with mean
$0$ and standard deviation, whose amplitude is parametersized by
the peak signal to noise (S/N) ratio.


\section{Inverse problem}\label{sec:inverse}
The inverse problem is given a noisy strongly lensed 
image, what are the lens parameters and what is the source brightness profile? 
In order to tackle this problem, 
we use the Bayesian approach and apply the technique to two sets 
of mock data. 

\subsection{Bayesian framework}
We use $\vec{s}$, $\vec{L}$ to denote source and lens parameters. 
The ray tracing and blurring operation is a linear operation, which we denote 
as $R$. We denote Gaussian noise as $\vec{N}$. Then the observations $\vec{O}$
can be written as 
\begin{equation}
\vec{O} = R(\vec{s}, \vec{L} ) + \vec{N}.
\end{equation}
We denote the priors of the lens and source parameters as
$p(\vec{L}, \vec{s})$, then the posterior distribution of the 
parameters given observation $\vec{O}$
is given by 
\begin{equation}\label{eq:posterior}
p(\vec{L},\vec{s}|\vec{O})\propto p(\vec{O}|\vec{L}, \vec{s})p(\vec{L}, \vec{s}),
\end{equation}
where the likelihood function is simply
\begin{equation}
p(\vec{O}|\vec{L}, \vec{s})
\propto \prod_{i=1}^{Nobs} \exp\left[ -(O_i - R(\vec{s},\vec{L})_i)^2/2\sigma_i^2\right].
\end{equation}
Here we treat $\sigma_i$ as uniform across the image, and takes the value
of $max(\vec{O})$ divided by the peak S/N value. This is because we  most 
likely can calibrate the telescope noise level quite well. 
We adopt weakly informative uniform priors within bounds 
that are physical and can be estimated from features of the image. 

In order to sample the posterior distribution in Eq. \ref{eq:posterior}, we use
the slice sampling method \citep{Neal03}. It requires 
very few parameter tuning. The only parameters are the typical 
width of the brackets in finding the slices, which we will 
summarize for each application. In general, keeping the width 
reasonably wide is a good idea, because it grantees finding 
a slice and the shrinking of the brackets
are exponential. 



\subsection{Appliation to an elliptical Gaussian source lensed by a SIE lensing potential}\label{sec:SIEGauss}

We generate a model with parameters listed in Table \ref{table:SIEGauss}. We adopt
a reasonable peak signal to noise level of $20$. The mock data with
blurring and noise is shown in the left panel of Figure \ref{fig:Gaussdata}.


\begin{table}[h]
\begin{centering}
\begin{tabular}{|c|c|c|c|c|}
\hline 
Parameter  & True Value  & Posterir mean  & Prior  & Width\tabularnewline
\hline 
\hline 
$b$  & $2.06$  & $2.05{}_{-0.01}^{+0.01}$  & $\mathcal{U}\sim(0,4)$  & $0.8$\tabularnewline
\hline 
$\vec{x}_{L}$  & $(0,0)$  & $(0.00{}_{-0.01}^{+0.01},0.00{}_{-0.01}^{+0.01})$  & $\mathcal{U}\sim([-2,2]^{2})$  & $[0.8,0.8]$\tabularnewline
\hline 
$\epsilon_{L}$  & $0.3$  & $0.30{}_{-0.01}^{+0.01}$  & $\mathcal{U}\sim(0,1)$  & $0.2$\tabularnewline
\hline 
$s_{L}$  & $0.1$  & $0.10{}_{-0.01}^{+0.01}$  & $\mathcal{U}\sim(0,1)$  & $0.1$\tabularnewline
\hline 
$\theta_{L}$  & $0.2\pi$  & $0.20{}_{-0.003}^{+0.003}\pi$  & $\mathcal{U}\sim(0,\pi)$  & $\pi/4$\tabularnewline
\hline 
$\vec{x}_{S}$  & $(0.1,0)$  & $(0.10{}_{-0.01}^{+0.01},0.00{}_{-0.01}^{+0.01})$  & $\mathcal{U}\sim([-2,-2],[2,2])$  & $[0.8,0.8]$\tabularnewline
\hline 
$\theta_{S}$  & $0.722\pi$  & $0.74{}_{-0.01}^{+0.01}\pi$  & $\mathcal{U}\sim(0,\pi)$  & $\pi/4$\tabularnewline
\hline 
$a_{S}$  & $0.2$  & $0.19{}_{-0.01}^{+0.01}$  & $\mathcal{U}\sim(0,2)$  & $0.4$\tabularnewline
\hline 
$q_{S}$  & $0.625$  & $0.67_{-0.03}^{+0.04}$  & $\mathcal{U}\sim(0,1)$  & $0.2$\tabularnewline
\hline 
$s_{0}$  & $1$  & $1.00{}_{-0.04}^{+0.05}$  & $\mathcal{U}\sim(0,5)$  & $1$\tabularnewline
\hline 
\end{tabular}
\par\end{centering}

\caption{Model parameter, mean of the posterior, choice of prior and width
of the slice sampling for data generated by 
a parameterized elliptical Gaussian lensed by a SIE potential presented in Sec. \ref{sec:SIEGauss}. } \label{table:SIEGauss}
\end{table}

We generate a total of $126000$ samples with priors and 
width for slice sampling summarized in Table \ref{table:SIEGauss}.
We discard the initial $6000$ samples and sub-sample the rest with a thinning ratio of $2$ in order
to reduce self-correlation of the sample. 
We plot the joint posterior distribution of selected
physical parameters, marginalized over non-fundamental parameters
such as the lens and source position and orientation in Figure \ref{fig:Gausscorner}.
The mean and 1-$\sigma$ intervals are indicated by the vertical black dashed lines
and the true value of the parameters are indicated by cyan solid lines. 
We also summarized all the posterior mean in Table \ref{table:SIEGauss}. 
We see that we successfully infer all parameters within 2-$\sigma$. 
The image corresponding to the MAP parameter is shown in the right panel of Figure \ref{fig:Gaussdata}, which shows excellent agreement with real data on the left panel. 

\begin{figure}[h]
\begin{centering}
\includegraphics[width=9cm]{SIEGauss_data}\includegraphics[width=9cm]{SIEGauss_MAP}
\par\end{centering}

\caption{Left panel: mock data including blurring and 
noise with peak S/N=20 for the parameterized elliptical Gaussian 
source lensed by a SIE. 
Right panel: image corresponding to the 
maximum of the posterior presented in Figure \ref{fig:Gaussscorner}.}\label{fig:Gaussdata}
\end{figure}


\begin{figure}[h]
\begin{centering}
\includegraphics[width=13cm]{SIEGauss_corner}\label{fig:Gausscorner}
\par\end{centering}

\caption{Joint posterior distribution of selected parameters after fitting 
the data in Sec. \ref{sec:SIEGauss}.  
The black dashed lines indicated the mean and 1-$\sigma$ confidence intervals
of the marginal posterior of each parameter. The true values of the parameters
are indicated by the cyan solid lines.
An animation of the slice sampling process can be viewed online at \protect\url{https://youtu.be/1Zhu_mSPe-U}}
\label{fig:Gaussscorner}.
\end{figure}






\subsection{Application to a pixelated source lensed by SIE\label{sec:SIEpixel}}
Encouraged by the positive results in the application to a parameterized 
source, we then applied the method to a more complicated source profile 
that takes free-form value at pixelated positions. 

\begin{table}[h]
\begin{centering}
\begin{tabular}{|c|c|c|c|c|}
\hline 
Parameter  & True Value  & Mean of posterior  & Prior  & width\tabularnewline
\hline 
\hline 
$b$  & $1.089$  & $1.088_{+0.001}^{-0.001}$  & $\mathcal{U}\sim(0,2.5)$  & $1$\tabularnewline
\hline 
$\vec{x}_{L}$  & $(0,0)$  & $\left(0.003_{+0.002}^{-0.002},-0.001_{+0.001}^{-0.001}\right)$  & $\mathcal{U}\sim([-1.25,1.25]^{2})$  & $(1,1)$\tabularnewline
\hline 
$\epsilon_{L}$  & $0.3$  & $0.298_{+0.002}^{-0.002}$  & $\mathcal{U}\sim(0,1)$  & $0.4$\tabularnewline
\hline 
$s_{L}$  & $0.05$  & $0.050_{+0.001}^{-0.001}$  & $\mathcal{U}\sim(0,0.2)$  & $0.2$\tabularnewline
\hline 
$\theta_{L}$  & $0.2\pi$  & $0.201_{+0.001}^{-0.001}$  & $\mathcal{U}\sim(0,\pi)$  & $\pi/2$\tabularnewline
\hline 
$S(\theta_{j=1\cdots144})$ & shown in Figure \ref{fig:pixelsource} & shown in Figure \ref{fig:meansource} & $\mathcal{U}\sim(0,8)$ & $5$\tabularnewline
\hline 
\end{tabular}
\par\end{centering}

\caption{Model parameter, mean of the posterior, choice of prior and width
of the slice sampling for parameters related to the pixelated source lensed by SIE data presented in Section \ref{sec:SIEpixel}. }\label{table:SIEpixel}
\end{table}


The source profile consists of two elliptical Gaussians pixelated
onto a $12\times12$ grid within the range $[-0.8'',0.8'']^{2}$ (shown
in the first panel of Figure \ref{fig:pixelsource}). We can see that the more general source profile introduced more complicated features in the image (second panel
of  \ref{fig:pixelsource}), such as double rings.

\begin{figure}[h]
\begin{centering}
\includegraphics[width=6cm]{SIEpixel_truesource}\includegraphics[width=6cm]{SIEpixel_data}\includegraphics[width=6cm]{SIEpixel_MAP}
\par\end{centering}

\caption{Panel 1: true source profile on a  $12\times12$ grid. 
Panel 2: mock data of the pixelated source lensed by a SIE with parameters summarized in Table \ref{table:SIEpixel}.
Panel 3: image corresponding to the MAP of the distribution 
shown in Figure \ref{fig:pixelcorner} and Figure \ref{fig:meansource}}\label{fig:pixelsource}
\end{figure}


We use a total of $108000$ samples and discard the first $1800$ samples. We
plot the joint posterior distribution of the lens parameters in Figure
\ref{fig:pixelcorner}. Again, we see that we recovered 
the lens parameters very well within 2-$\sigma$ interval. 
The mean and standard deviation of the posterior of the
pixel values are shown in the first two panels of Figure \ref{fig:meansource}.
We also compare the mean source profile to the true source profile (Figure \ref{fig:pixelsource}) and plot the error in the third panel of Figure \ref{fig:meansource}. We successfully recover the source profile 
with very errors that are at most $2\sigma$. 

\begin{figure}[h]
\begin{centering}
\includegraphics[width=13cm]{SIEpixel_corner_full}
\par\end{centering}

\caption{Joint posterior distribution of the lens parameters fitted from 
data in Section \ref{sec:SIEpixel}. An animation
of the slice sampling process can be viewed online at \protect\url{https://youtu.be/lhWx1txiHM4}.}\label{fig:pixelcorner}
\end{figure}


\begin{figure}[h]
\begin{centering}
\includegraphics[width=6cm]{SIEpixel_meansource}\includegraphics[width=6cm]{SIEpixel_std}\includegraphics[width=6cm]{SIEpixel_error}
\par\end{centering}

\caption{Panel 1: mean of the posterior of the source pixel values; panel 2:
standard deviation of the posterior of the source pixel values; panel
3: error of the mean source with respect to the true source. }\label{fig:meansource}
\end{figure}



\bibliographystyle{apj}
\bibliography{mybib}
 % list here all the bibliographies that % you need. 
\end{document}
