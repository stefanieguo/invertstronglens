# -*- coding: utf-8 -*-
"""
Created on Tue May 10 12:56:03 2016

@author: xguo
"""

#from IPython import get_ipython
#get_ipython().magic('reset -sf')


import numpy as np
import matplotlib.pyplot as plt
from misc import rotateByTheta, return_ellipse, return_re
#from matplotlib.colors import LogNorm, PowerNorm

SMALLR = 1e-15
SMALLMU = 3e-2

class SIS():
    def __init__(self, modelparams, verbose = False):
        re, xLc, yLc = modelparams
        #Einstein radius
        self.re = re
        self.xa = self.re
        self.xb = self.re
        self.thetaL = 0
        if (verbose):
            print("SIS Lens Model:")
            print("Einstein radius = {:.2f}".format(self.re))
        self.xLc = xLc
        self.yLc = yLc
        
    def deflect(self, xImage, yImage, verbose=False):
        #shift to lens center frame
        R = np.sqrt( (xImage-self.xLc)**2 + (yImage-self.yLc)**2)
        R[R<SMALLR] = SMALLR
        
        xS = xImage - self.re*(xImage-self.xLc)/R
        yS = yImage - self.re*(yImage-self.yLc)/R
        
        oneOvermu = np.abs(1. - self.re/R)
        if verbose:
            print(np.sum(oneOvermu<SMALLMU)*1.0/R.size)
        oneOvermu[oneOvermu<SMALLMU] = SMALLMU  
        
        return xS, yS, 1/oneOvermu
        
    def drawCrit(self, color = 'r', linestyle = '-', linewidth = 2):
        "assuming s=0 in this case, draw the surface of mu^-1 = 0"
        x, y = return_ellipse(self.re, self.re, self.xLc, self.yLc, 0)
        return plt.plot(x, y, color,linestyle = linestyle, linewidth = linewidth)

    def drawCaustic(self):
        x, y = return_ellipse(self.re, self.re, self.xLc, self.yLc, 0)
        xc, yc, _ = self.deflect(x, y)
        return plt.plot(xc, yc, 'y')
        
class SIE():
    def __init__(self, modelparam, verbose = False):
        re, xLc, yLc, ellip, s, thetaL = modelparam
        self.thetaL = thetaL
        self.xLc = xLc
        self.yLc = yLc
        
        self.s = s
        
        self.re = re
        self.ellip = ellip
        # aspect ratio
        self.q =  np.sqrt( (1. - self.ellip)/(1. + self.ellip)   )
        # minor axis
        self.xb = self.re
        # major axis
        self.xa = self.re/self.q   
        
        if (verbose):
            print("SIE Lens Model:")
            print("Einstein radius = {:.2f}".format(self.re))
    
    def deflect(self, xImage, yImage, verbose = False):
        #to avoid blowing up with too small ellipticity
        if (self.ellip<1e-5):
            R = np.sqrt( (xImage-self.xLc)**2 + (yImage-self.yLc)**2)
            R[R<SMALLR] = SMALLR
            
            xS = xImage - self.re*(xImage-self.xLc)/R
            yS = yImage - self.re*(yImage-self.yLc)/R
            
            oneOvermu = np.abs(1. - self.re/R)
            if (verbose):
                print(np.sum(oneOvermu<SMALLMU)*1.0/R.size)
            oneOvermu[oneOvermu<SMALLMU] = SMALLMU  
            
            return xS, yS, 1/oneOvermu
        else:
            xL, yL = rotateByTheta(xImage, yImage, -self.thetaL)
            xLc0, yLc0 = rotateByTheta(self.xLc, self.yLc, -self.thetaL)
            xL -= xLc0
            yL -= yLc0
            
            #auxilary variables
            psi = np.sqrt( self.q**2*(self.s**2+xL**2) + yL**2)
            if (self.s==0):
                psi[psi< SMALLR] = SMALLR
            
            #magnification
            oneOvermu = np.abs( 1.- self.re/psi - self.re**2*self.s/(psi*( (psi+self.s)**2+(1-self.q**2)*xL**2 )) )
            if (xImage.ndim>1 and verbose):            
                print('cap mu in {:.2}% cells'.format(np.sum(oneOvermu<SMALLMU)*100.0/psi.size))
            oneOvermu[oneOvermu<SMALLMU] = SMALLMU        
            
            #deflect the rays
            xL -= self.re/np.sqrt(1-self.q**2)*np.arctan(xL*np.sqrt(1-self.q**2)/(psi+self.s))
            yL -= self.re/np.sqrt(1-self.q**2)*np.arctanh(yL*np.sqrt(1-self.q**2)/(psi+self.q**2*self.s))
            
            xS, yS = rotateByTheta(xL + xLc0, yL + yLc0, self.thetaL)
            
            return xS, yS, 1./oneOvermu
        
    def drawCrit(self, color = 'r',linestyle = '-', linewidth = 2):
        "assuming s=0 in this case, draw the surface of mu^-1 = 0"
        x, y = return_ellipse(self.xa, self.xb, self.xLc, self.yLc, self.thetaL)
        return plt.plot(x, y, color=color, linestyle = linestyle, linewidth = linewidth)

    def drawCaustic(self, color = 'y'):
        x, y = return_ellipse(self.xa, self.xb, self.xLc, self.yLc, self.thetaL)
        xc, yc, _ = self.deflect(x, y)
        return plt.plot(xc, yc, color)
        
if __name__ == '__main__':
    zL = 0.3
    zS = 0.7
    xLc = 1.0
    yLc = 0.5
    sigma = 400
    thetaL = 0.2*np.pi 
    ellipL = 0.5
    sL  = 0
    #lens  = SIE([zL, zS, sigma, xLc, yLc, ellipL, sL, thetaL], True)
    lens  = SIS([return_re(zL, zS, sigma), xLc, yLc], True)    
    plt.figure(1)
    plt.clf()
    lens.drawCrit()
    lens.drawCaustic()
    plt.plot(lens.xLc, lens.yLc, '*', color = 'm', markersize=20)    
    plt.axes().set_aspect('equal', 'datalim')
    
    lens  = SIE([return_re(zL, zS, sigma), xLc, yLc, ellipL, sL, thetaL], True)
    plt.figure(2)
    plt.clf()
    lens.drawCrit()
    lens.drawCaustic()
    plt.plot(lens.xLc, lens.yLc, '*', color = 'm', markersize=20)    
    plt.axes().set_aspect('equal', 'datalim')
    plt.show()    

    