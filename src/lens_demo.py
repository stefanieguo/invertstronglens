# -*- coding: utf-8 -*-
"""
Created on Tue May 10 15:55:27 2016

@author: xguo
"""
import numpy as np
import matplotlib.pyplot as plt
import source_profiles as srcprf
import lenses as lenses
from misc import draw_ellipse, draw_rect, return_re
from forward_problem import LensFunction
from matplotlib.colors import PowerNorm
import functools

plt.rcParams['image.cmap'] = 'inferno'
plt.rcParams['font.size'] = 16


    
if __name__ == '__main__':
    zL = 0.3
    zS = 0.6
    xLc = 0#1.0
    yLc = 0#0.5
    sigma = 450
    thetaL = 0.2*np.pi 
    ellipL = 0.3
    sL  = 0
    lens  = lenses.SIE([return_re(zL, zS, sigma), xLc, yLc, ellipL, sL, thetaL], True)
    
   
    xmin, xmax = -5, 5
    ymin, ymax = -5, 5
    sxmin, sxmax = -1.2, 1.2
    symin, symax = -1.2, 1.2
    nx = 168
    X, Y = np.meshgrid( np.linspace(xmin, xmax, nx), np.linspace(ymin,ymax, nx)  )   


    plt.figure(1)
    plt.clf()
    xSc, ySc = 0.6*1.1, 0.5*1.1
    sigma1 = 0.16
    qS = 0.6
    thetaS = (-20)*np.pi/180   
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=sigma1, qS=qS, s0=1)
    Image = LensFunction(X, Y, lens, f)
    ax1 = plt.pcolor(X,Y, Image )
    ax1.set_norm(PowerNorm(gamma=0.4))
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.text(-4, -4, 'cusp',color = 'w', fontsize = 20)
    plt.colorbar()
    lens.drawCrit('r',linestyle = '-.')
    draw_ellipse(sigma1*2, sigma1*qS*2, xSc, ySc, thetaS, 'y')
    draw_rect(
        sxmin,  symin,
        sxmax - sxmin,
        symax - symin , 'c','--'   )
    lens.drawCaustic('r')

    
    
    plt.figure(2)
    plt.clf()
    xSc, ySc = -0.5, 0
    sigma1 = 0.16
    qS = 0.6
    thetaS = (40+32)*np.pi/180   
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=sigma1, qS=qS, s0=1)
    Image = LensFunction(X, Y, lens, f)
    
    ax1 = plt.pcolor(X,Y, Image )
    ax1.set_norm(PowerNorm(gamma=0.8))
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.text(-4, -4, 'fold',color = 'w', fontsize = 20)
    plt.colorbar()
    lens.drawCrit('r', linestyle = '-.')
    draw_ellipse(sigma1*2, sigma1*qS*2, xSc, ySc, thetaS, 'y')
    draw_rect(
        sxmin,  symin,
        sxmax - sxmin,
        symax - symin , 'c','--'   )
    lens.drawCaustic('r')
    
    
   
    plt.figure(3)
    plt.clf()
    xSc, ySc =  0.1, 0.
    sigma1 = 0.16#0.125
    qS = 0.6
    thetaS = (40+190)*np.pi/180   
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=sigma1, qS=qS, s0=1)
    Image = LensFunction(X, Y, lens, f)
    
    plt.pcolor(X,Y, Image )
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.text(-4, -4, 'cross',color = 'w', fontsize = 20)
    plt.colorbar()
    lens.drawCrit('r',linestyle = '-.')
    draw_ellipse(sigma1*2, sigma1*qS*2, xSc, ySc, thetaS, 'y')
    draw_rect(
        sxmin,  symin,
        sxmax - sxmin,
        symax - symin , 'c','--'   )
    lens.drawCaustic('r')
    
    
    plt.figure(4)
    plt.clf()
    xSc, ySc = 0.24, 0.76
    sigma1 = 0.16
    qS = 0.6
    thetaS = (20)*np.pi/180   
    f = functools.partial(srcprf.EllipticalGaussian, xSc = xSc, ySc=ySc, thetaS=thetaS, aS=sigma1, qS=qS, s0=1)
    Image = LensFunction(X, Y, lens, f)
    
    ax1 = plt.pcolor(X,Y, Image )
    plt.axis([X.min(), X.max(), Y.min(), Y.max()])
    plt.xlabel('x [arcsec]')
    plt.ylabel('y [arcsec]')
    plt.text(-4, -4, '2-image',color = 'w', fontsize = 20)
    plt.colorbar()
    lens.drawCrit('r',linestyle = '-.')
    draw_ellipse(sigma1*2, sigma1*qS*2, xSc, ySc, thetaS, 'y')
    draw_rect(
        sxmin,  symin,
        sxmax - sxmin,
        symax - symin , 'c','--'   )
    lens.drawCaustic('r')
    
    plt.show()